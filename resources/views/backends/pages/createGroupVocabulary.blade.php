@extends('backends.layouts.create')

@section('title','Create Group vocabulary')

@section('body_class','create-group-vocabulary')

@section('entry_title', 'Tạo nhóm câu hỏi trắc nghiệm')

@section('main_content')
	{!!
		Form::open([
			'route'=>['group_vocabulary.store'],
			'method'=>'POST',
			'class'=>'post_form',
			'files'=>true
		])
	!!}

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					{!! Form::label('group_vocabulary_title', 'Group vocabulary') !!}
					{!! Form::text('group_vocabulary_title',null,['id'=>'group_vocabulary_title','class'=>'form-control','placeholder'=>'Group vocabulary title']) !!}	
				</div>
				<div class="form-group">
					{!! Form::label('group_quiz_id', 'Group quiz id') !!}
					{!! Form::text('group_quiz_id',null,['id'=>'group_quiz_id','class'=>'form-control','placeholder'=>'']) !!}	
				</div>
				<div class="form-group">
					{!! Form::label('thumbnail', 'Thumbnail'); !!}
					<div class="wrap-thumb">       
					   <input class="thumbUpload" type="file" name="group_vocabulary_thumbnail" />
					   <div class="image-holder"> </div>
					 </div>
				</div>	
				<div class="form-group text-right">
					{!! Form::button("Save",['class'=>'btn btn-primary','type'=>'submit']) !!}
				</div>		
			</div>
		</div>

	{!! Form::close() !!}    				
@endsection