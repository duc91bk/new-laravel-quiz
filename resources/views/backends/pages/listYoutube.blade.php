@extends('backends.layouts.master')

@section('title', 'List youtube')

@section('body_class', 'page_list')

@section('main_content')
	<div class="content-wrapper">		
        <section class="content">
        	<div class="row">
        		<div class="col-md-12">
        			<div class="box box-primary">
		                <div class="box-header with-border">
		                  <h3 class="box-title">Danh sach youtube</h3>
		                  
		                </div><!-- /.box-header -->
		                <div class="box-body no-padding">
		                  <div class="post-controls clearfix">
		                    
		                    <div class="pull-right">
		                      <div class="posts-count pull-left">
		                      	{{ $youtubes->total() }} items
		                      </div>
		                      <div class="pull-left">
		                        {!! $youtubes->links() !!}
		                      </div><!-- /.btn-group -->
		                    </div><!-- /.pull-right -->
		                  </div>
		                  <div class="table-responsive ">
		                    <table class="table table-hover table-striped">
		                    	<thead>
		                    		<tr>
		                    			<th class="cat-id" width="60">ID</th>
		                    			<th class="cat-user" width="200">User name</th>
		                    			<th class="cat-name" colspan="3">Youtube title</th>
		                    		</tr>
		                    	</thead>
		                      <tbody>
		                        @foreach($youtubes as $youtube)
									<tr>
			                          <td class="cat-id">{{ $youtube->id }}</td>
			                          <td class="cat-user">{{ $youtube->name }}</td>
			                          <td class="cat-name"><a href="/"> {{ $youtube->youtube_title }} </a></td>
										@if(Auth::user()->level < 3)
				                          <td class="cat-edit" width="80"><a href="{{ route('youtube.edit', $youtube->id)}}">Edit</a></td>
				                          <td class="cat-delete" width="80"><a href="{{ route('youtube.destroy', $youtube->id)}}" class="text-danger">Delete</a></td>
			                          	@endif

			                        </tr>
								@endforeach								
		                      </tbody>
		                    </table><!-- /.table -->
		                  </div><!-- /.mail-box-messages -->
		                </div><!-- /.box-body -->
		                <div class="box-footer no-padding">
		                  <div class="post-controls clearfix">		                    
		                    <div class="pull-right">
		                      <div class="posts-count pull-left">
		                      	{{ $youtubes->total() }} items
		                      </div>
		                      <div class="pull-left">
		                        {!! $youtubes->links() !!}
		                      </div><!-- /.btn-group -->
		                    </div><!-- /.pull-right -->
		                  </div>
		                </div>
		              </div><!-- /. box -->
        		</div>
        	</div>
			
		</section>
		<!-- content -->

	</div>
	<!-- content-wrapper -->
@endsection