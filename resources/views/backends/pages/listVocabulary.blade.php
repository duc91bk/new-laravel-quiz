@extends('backends.layouts.master')

@section('title', 'List vocabulary')

@section('body_class', 'page_list')

@section('main_content')
	<div class="content-wrapper">		
        <section class="content">
        	<div class="row">
        		<div class="col-md-12">
        			<div class="box box-primary">
		                <div class="box-header with-border">
		                  <h3 class="box-title">Danh sach chuyen muc</h3>
		                  
		                </div><!-- /.box-header -->
		                <div class="box-body no-padding">
		                  <div class="post-controls clearfix">
		                    
		                    <div class="pull-right">
		                      <div class="posts-count pull-left">
		                      	{{ $vocabularies->total() }} items
		                      </div>
		                      <div class="pull-left">
		                        {!! $vocabularies->render() !!}
		                      </div><!-- /.btn-group -->
		                    </div><!-- /.pull-right -->
		                  </div>
		                  <div class="table-responsive ">
		                    <table class="table table-hover table-striped">
		                    	<thead>
		                    		<tr>
		                    			<th class="vocabulary-id" >ID</th>
		                    			<th class="vocabulary-user" >User</th>
		                    			<th class="vocabulary-name" >Vocabulary</th>
		                    			<th class="vocabulary-ipa" >IPA</th>
		                    			<th class="vocabulary-thumbnail" >Thumbnail</th>
		                    			<th class="vocabulary-content" >Content</th>
		                    		</tr>
		                    	</thead>
		                      <tbody>
		                        @foreach($vocabularies as $vocabulary)
									<tr>
			                          <td class="vocabulary-id">{{ $vocabulary->id }}</td>
			                          <td class="vocabulary-user">{{ $vocabulary->name }}</td>
			                          <td class="vocabulary-name"><a href="/"> {{ $vocabulary->vocabulary_title }} </a></td>
			                          <td class="vocabulary-ipa">
			                          	{{ $vocabulary->vocabulary_ipa }}
			                          	<br>
			                          	<audio controls style="width: 46px">
							                <source src="{{ $vocabulary->vocabulary_media }}" type="audio/mpeg">
							                Trình duyệt của bạn không hỗ trợ audio    
							            </audio>
			                          </td>
			                          <td class="vocabulary-thumbnail"><img src="{{ $vocabulary->vocabulary_thumbnail }}" alt=""></td>
			                          <td class="vocabulary-content">{!! $vocabulary->vocabulary_content !!}</td>
										@if(Auth::user()->level < 3)
				                          <td class="vocabulary-edit" width="80"><a href="{{ route('vocabulary.edit', $vocabulary->id)}}">Edit</a></td>
				                          <td class="vocabulary-delete" width="80"><a href="{{ route('vocabulary.destroy', $vocabulary->id)}}" class="text-danger">Delete</a></td>
			                          	@endif
			                        </tr>
								@endforeach								
		                      </tbody>
		                    </table><!-- /.table -->
		                  </div><!-- /.mail-box-messages -->
		                </div><!-- /.box-body -->
		                <div class="box-footer no-padding">
		                  <div class="post-controls clearfix">		                    
		                    <div class="pull-right">
		                      <div class="posts-count pull-left">
		                      	{{ $vocabularies->total() }} items
		                      </div>
		                      <div class="pull-left">
		                        {!! $vocabularies->render() !!}
		                      </div><!-- /.btn-group -->
		                    </div><!-- /.pull-right -->
		                  </div>
		                </div>
		              </div><!-- /. box -->
        		</div>
        	</div>
			
		</section>
		<!-- content -->

	</div>
	<!-- content-wrapper -->
@endsection