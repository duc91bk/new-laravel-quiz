@extends('backends.layouts.master')

@section('title', 'List cat')

@section('body_class', 'page_list')

@section('main_content')
	<div class="content-wrapper">		
        <section class="content">
        	<div class="row">
        		<div class="col-md-12">
        			<div class="box box-primary">
		                <div class="box-header with-border">
		                  <h3 class="box-title">Danh sach chuyen muc</h3>
		                  
		                </div><!-- /.box-header -->
		                <div class="box-body no-padding">
		                  <div class="post-controls clearfix">
		                    
		                    <div class="pull-right">
		                      <div class="posts-count pull-left">
		                      	{{ $cats->total() }} items
		                      </div>
		                      <div class="pull-left">
		                        {!! $cats->render() !!}
		                      </div><!-- /.btn-group -->
		                    </div><!-- /.pull-right -->
		                  </div>
		                  <div class="table-responsive ">
		                    <table class="table table-hover table-striped">
		                    	<thead>
		                    		<tr>
		                    			<th class="cat-user" width="200">User</th>
		                    			<th class="cat-name" colspan="3">Cat Name</th>
		                    		</tr>
		                    	</thead>
		                      <tbody>
		                        @foreach($cats as $cat)
									<tr>
			                          	<td class="cat-user">{{ $cat->name }}</td>
			                          	<td class="cat-name"><a href="/"> {{ $cat->cat_title }} </a></td>
				                        <td class="cat-edit" width="80">
				                        	@if(Auth::user()->id = $cat->user_id || (Auth::user()->usre_level > 3 && Auth::user()->user_level > $cat->user_level))
				                        	<a href="{{ route('cat.edit', $cat->id)}}">Edit</a> 
				                        	@endif
				                        </td>
				                        <td class="cat-delete" width="80">
				                        	@if(Auth::user()->id = $cat->user_id || (Auth::user()->usre_level > 3 && Auth::user()->user_level > $cat->user_level))
				                        	<a href="{{ route('cat.destroy', $cat->id)}}" class="text-danger">Delete</a>
				                        	@endif
				                        </td>

			                        </tr>
								@endforeach								
		                      </tbody>
		                    </table><!-- /.table -->
		                  </div><!-- /.mail-box-messages -->
		                </div><!-- /.box-body -->
		                <div class="box-footer no-padding">
		                  <div class="post-controls clearfix">		                    
		                    <div class="pull-right">
		                      <div class="posts-count pull-left">
		                      	{{ $cats->total() }} items
		                      </div>
		                      <div class="pull-left">
		                        {!! $cats->render() !!}
		                      </div><!-- /.btn-group -->
		                    </div><!-- /.pull-right -->
		                  </div>
		                </div>
		              </div><!-- /. box -->
        		</div>
        	</div>			
		</section>
		<!-- content -->

	</div>
	<!-- content-wrapper -->
@endsection