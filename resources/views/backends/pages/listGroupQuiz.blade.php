@extends('backends.layouts.master')

@section('title', 'List group quiz')

@section('body_class', 'page_list')

@section('main_content')
	<div class="content-wrapper">
		<section class="content-header">
			<h1>
				Danh sach group quizzes
			</h1>
        </section>
        <section class="content">
        	<div class="row">
        		<div class="col-md-12">
        			<div class="box box-primary">
		                <div class="box-header with-border">
		                  <h3 class="box-title">Inbox</h3>
		                  <div class="box-tools pull-right">
		                    <div class="has-feedback">
		                      <input type="text" class="form-control input-sm" placeholder="Search Mail" />
		                      <span class="glyphicon glyphicon-search form-control-feedback"></span>
		                    </div>
		                  </div><!-- /.box-tools -->
		                </div><!-- /.box-header -->
		                <div class="box-body no-padding">
		                  <div class="post-controls clearfix">
		                    		                    
		                    <div class="pull-right">
		                      <div class="posts-count pull-left">
		                      	{{ $group_quizzes->total() }} items
		                      </div>
		                      <div class="pull-left">
		                        {!! $group_quizzes->render() !!}
		                      </div><!-- /.btn-group -->
		                    </div><!-- /.pull-right -->
		                  </div>
		                  <div class="table-responsive ">
		                    <table class="table table-hover table-striped">
		                    	<thead>
		                    		<tr>
		                    			<td class="group_quizzes-id">ID</td>
		                    			<td class="group_quizzes-user">User</td>
										<td class="group_quizzes-name">Name</td>
										<td class="group_quizzes-number">Number quizzes</td>
		                    		</tr>
		                    	</thead>
		                      <tbody>
		                        @foreach($group_quizzes as $group_quiz)
									<tr>
			                          <td class="group_quizzes-id">{{ $group_quiz->id }}</td>
			                          <td class="group_quizzes-user">{{ $group_quiz->name }}</td>
			                          <td class="group_quizzes-name"><a href="{{ route('quizzes.index', $group_quiz->id) }}"> {{ $group_quiz->group_quiz_title }} </a></td>
			                          <td class="group_quizzes-number">{{ $ar_number_quiz["$group_quiz->id"] }}</td>
			                          <td class="group_quizzes-edit"><a href="{{ route('group_quiz.edit', $group_quiz->id)}}">Edit</a></td>
			                          <td class="group_quizzes-delete"><a href="{{ route('group_quiz.destroy', $group_quiz->id)}}" class="text-danger">Delete</a></td>
			                        </tr>
								@endforeach								
		                      </tbody>
		                    </table><!-- /.table -->
		                  </div><!-- /.mail-box-messages -->
		                </div><!-- /.box-body -->
		                <div class="box-footer no-padding">
		                  <div class="post-controls clearfix">
		                    		                    
		                    <div class="pull-right">
		                      <div class="posts-count pull-left">
		                      	{{ $group_quizzes->total() }} items
		                      </div>
		                      <div class="pull-left">
		                        {!! $group_quizzes->render() !!}
		                      </div><!-- /.btn-group -->
		                    </div><!-- /.pull-right -->
		                  </div>
		                </div>
		              </div><!-- /. box -->
        		</div>
        	</div>
			
		</section>
		<!-- content -->

	</div>
	<!-- content-wrapper -->
@endsection