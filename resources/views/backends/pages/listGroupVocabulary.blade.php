@extends('backends.layouts.master')

@section('title', 'List group groupVocabulary')

@section('body_class', 'page_list')

@section('main_content')
	<div class="content-wrapper">		
        <section class="content">
        	<div class="row">
        		<div class="col-md-12">
        			<div class="box box-primary">
		                <div class="box-header with-border">
		                  <h3 class="box-title">Danh sách nhóm từ vựng</h3>
		                  
		                </div><!-- /.box-header -->
		                <div class="box-body no-padding">
		                  <div class="post-controls clearfix">
		                    
		                    <div class="pull-right">
		                      <div class="posts-count pull-left">
		                      	{{ $groupVocabularies->total() }} items
		                      </div>
		                      <div class="pull-left">
		                        {!! $groupVocabularies->links() !!}
		                      </div><!-- /.btn-group -->
		                    </div><!-- /.pull-right -->
		                  </div>
		                  <div class="table-responsive ">
		                    <table class="table table-hover table-striped">
		                    	<thead>
		                    		<tr>
		                    			<th class="groupVocabulary-id" >ID</th>
		                    			<th class="groupVocabulary-user" >User</th>
		                    			<th class="groupVocabulary-name" >Group vocabulary</th>
		                    		</tr>
		                    	</thead>
		                      <tbody>
		                        @foreach($groupVocabularies as $groupVocabulary)
									<tr>
			                          <td class="groupVocabulary-id">{{ $groupVocabulary->id }}</td>
			                          <td class="groupVocabulary-user">{{ $groupVocabulary->name }}</td>
			                          <td class="groupVocabulary-name"><a href="{{ route('group_vocabulary.show', $groupVocabulary->id) }}"> {{ $groupVocabulary->group_vocabulary_title }} </a></td>
			                          
			                          
										@if(Auth::user()->id == $groupVocabulary->user_id || ( Auth::user()->user_level > $groupVocabulary->user_level  && Auth::user()->user_level > 3 )) 
				                          <td class="groupVocabulary-edit" width="80"><a href="{{ route('group_vocabulary.edit', $groupVocabulary->id)}}">Edit</a></td>
				                          <td class="groupVocabulary-delete" width="80"><a href="{{ route('group_vocabulary.destroy', $groupVocabulary->id)}}" class="text-danger">Delete</a></td>
			                          	@endif
			                        </tr>
								@endforeach								
		                      </tbody>
		                    </table><!-- /.table -->
		                  </div><!-- /.mail-box-messages -->
		                </div><!-- /.box-body -->
		                <div class="box-footer no-padding">
		                  <div class="post-controls clearfix">		                    
		                    <div class="pull-right">
		                      <div class="posts-count pull-left">
		                      	{{ $groupVocabularies->total() }} items
		                      </div>
		                      <div class="pull-left">
		                        {!! $groupVocabularies->links() !!}
		                      </div><!-- /.btn-group -->
		                    </div><!-- /.pull-right -->
		                  </div>
		                </div>
		              </div><!-- /. box -->
        		</div>
        	</div>
			
		</section>
		<!-- content -->

	</div>
	<!-- content-wrapper -->
@endsection