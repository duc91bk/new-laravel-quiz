@extends('backends.layouts.create')

@section('title','Create youtube')

@section('body_class','create-youtube')

@section('entry_title', 'Tạo youtube')

@section('main_content')
	{!!
		Form::open([
			'route'=>['youtube.store'],
			'method'=>'POST',
			'class'=>'post_form'
		])
	!!}

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					{!! Form::label('youtube_title', 'Youtube title') !!}
					{!! Form::text('youtube_title',null,['id'=>'youtube_title','class'=>'form-control','placeholder'=>'Youtube title']) !!}	
				</div>
				<div class="form-group">
					{!! Form::label('youtube_url', 'Youtube url') !!}
					{!! Form::text('youtube_url',null,['id'=>'youtube_url','class'=>'form-control','placeholder'=>'Youtube url']) !!}	
				</div>
				<div class="form-group">
					{!! Form::label('youtube_en', 'Youtube En') !!}
					{!! Form::text('youtube_en',null,['id'=>'youtube_en','class'=>'form-control','placeholder'=>'Youtube En']) !!}	
				</div>
				<div class="form-group">
					{!! Form::label('youtube_vn', 'Youtube Vn') !!}
					{!! Form::text('youtube_vn',null,['id'=>'youtube_vn','class'=>'form-control','placeholder'=>'Youtube Vn']) !!}	
				</div>
				<div class="form-group text-right">
					{!! Form::button("Save",['class'=>'btn btn-primary','type'=>'submit']) !!}
				</div>		
			</div>
		</div>

	{!! Form::close() !!}    				
@endsection