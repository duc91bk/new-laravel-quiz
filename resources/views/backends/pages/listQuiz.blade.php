@extends('backends.layouts.master')

@section('title', 'List quiz')

@section('body_class', 'page_list')

@section('main_content')
	<div class="content-wrapper">
		<section class="content-header">
			<h1>
				{{ $group_quiz_title }}
			</h1>
        </section>
        <section class="content">
              
            <div class="list_quizzes">
            	<?php $count=0; ?>
                @foreach($quizzes as $quiz)
					<?php $count++; ?>
                	<div class="box box-solid">
		                <div class="box-header with-border">
		                  <h3 class="box-title">Câu {{ $count }} : {{ $quiz->quiz_title }}</h3>
		                  <div class="box-tools pull-right">
			                <a href="#" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></a>
			                <a href="{{ route('quiz.edit', $quiz->id) }}" class="btn btn-box-tool"><i class="fa fa-edit"></i></a>
			                <a href="{{ route('quiz.destroy', $quiz->id) }}" class="btn btn-box-tool" ><i class="fa fa-remove"></i></a>
			              </div>
		                </div><!-- /.box-header -->
		                <div class="box-body">
							<div class="row">
								<?php 
									foreach ($answers as $answer) {

										if ($answer->quiz_id == $quiz->id) { ?>
											
											<div class="col-md-3">												
												<?php 	
													if ($answer->answer_is_correct == 1) {
														echo '<div class="small-box bg-aqua">';
													} else {
														echo '<div class="small-box box box-solid box-info">';
													}
												?>

													<div class="inner">
														<p class="quiz-answer">{{ $answer->answer_title }}</p>
														<div class="quiz-thumbnail"><img src="{{ $answer->answer_thumbnail }}" alt=""></div>
													</div>										
												</div>
											</div>

										<?php }
									}
								?>
							</div>
							
		                </div><!-- /.box-body -->
		            </div>                	                   
				@endforeach								
            </div><!-- list quizzes -->
			
		</section>
		<!-- content -->

	</div>
	<!-- content-wrapper -->
@endsection