@extends('backends.layouts.create')

@section('title','Create Cat')

@section('body_class','create-cat')

@section('entry_title', 'Tạo chuyên mục mới')

@section('main_content')
	{!!
		Form::open([
			'route'=>['cat.store'],
			'method'=>'POST',
			'class'=>'post_form'
		])
	!!}

		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					{!! Form::label('cat_title', 'Category') !!}
					{!! Form::text('cat_title',null,['id'=>'cat_title','class'=>'form-control','placeholder'=>'Category']) !!}	
				</div>
				<div class="form-group text-right">
					{!! Form::button("Save",['class'=>'btn btn-primary','type'=>'submit']) !!}
				</div>		
			</div>
		</div>

	{!! Form::close() !!}    				
@endsection