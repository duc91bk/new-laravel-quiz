@extends('backends.layouts.create')

@section('title','Create vocabulary')

@section('body_class','create-vocabulary')

@section('entry_title', 'Tạo từ vựng mới')

@section('main_content')
	{!!
		Form::open([
			'route'=>['vocabulary.store'],
			'method'=>'POST',
			'class'=>'post_form',
			'files' =>true
		])
	!!}

		<div class="row">
			<div class="col-md-9">
				<div class="form-group">
					{!! Form::label('vocabulary_title', 'vocabulary title') !!}
					{!! Form::text('vocabulary_title',null,['id'=>'vocabulary_title','class'=>'form-control','placeholder'=>'vocabulary title']) !!}	
				</div>
				<div class="form-group">
					{!! Form::label('vocabulary_ipa', 'IPA') !!}
					{!! Form::text('vocabulary_ipa',null,['id'=>'vocabulary_ipa','class'=>'form-control','placeholder'=>'IPA']) !!}	
				</div>	
				<div class="form-group">
					{!! Form::label('post_content', 'Content'); !!}
					{!! Form::textarea('vocabulary_content',null,['id'=>'post_content','class'=>'form-control','placeholder'=>'Content']) !!}
				</div>
				
			</div>
			<!-- col 9 -->
			<div class="col-md-3">
				<div class="form-group">
					{!! Form::label('thumbnail', 'Thumbnail'); !!}
					<div class="wrap-thumb">       
					   <input class="thumbUpload" id="thumbnail" type="file" name="vocabulary_thumbnail" />
					   <div class="image-holder"> </div>
					 </div>
				</div>	
				<div class="form-group">
					{!! Form::label('audio', 'Audio'); !!}
					<input type="file" name="vocabulary_media" id="audio" />
				</div>
				<div class="form-group">
					{!! Form::label('group_vocabulary_id', 'Group vocabulary id') !!}
					{!! Form::text('group_vocabulary_id',null,['id'=>'group_vocabulary_id','class'=>'form-control','placeholder'=>'']) !!}	
				</div>
				<div class="form-group text-right">
					{!! Form::button("Save",['class'=>'btn btn-primary','type'=>'submit']) !!}
				</div>		
			</div>
		</div>

	{!! Form::close() !!}    				
@endsection