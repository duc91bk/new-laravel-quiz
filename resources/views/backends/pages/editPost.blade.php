@extends('backends.layouts.create')

@section('title','Create Post')

@section('entry_title', 'Chỉnh sửa bài viết')

@section('main_content')	
	{!!
		Form::model($post,[
			'route'=>['post.update',$post->id],
			'method'=>'PUT',
			'class'=>'post_form',
			'files'=>true
		])
	!!}
		<?php if(isset($status_action_post) && !empty($status_action_post)) { ?>
		<div class="alert alert-success alert-dismissible" role="alert">
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  {{ $status_action_post }}
		</div>
		<?php } ?>
		<div class="row">
			<div class="col-md-9">
				<div class="form-group">
					{!! Form::label('post_title', 'Title') !!}
					{!! Form::text('post_title',null,['id'=>'post_title','class'=>'form-control','placeholder'=>'Title']) !!}	
				</div>		
				<div class="form-group">
					{!! Form::label('post_content', 'Content'); !!}
					{!! Form::textarea('post_content',null,['id'=>'post_content','class'=>'form-control','placeholder'=>'Content']) !!}
				</div>
				
				
			</div>
			<div class="col-md-3">
				<div class="form-group">
					{!! Form::label('cat_id', 'Category'); !!}
					{!! Form::select('cat_id', $arCats, null, ['class'=>'form-control']); !!}
				</div>
				<div class="form-group">
					{!! Form::label('group_quiz_id', 'Group quizzes id') !!}					
					{!! Form::text('group_quiz_id',null,['id'=>'group_quiz_id','class'=>'form-control','placeholder'=>'Group quizzes id']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('youtube_id', 'Youtube id') !!}
					{!! Form::text('youtube_id',null,['id'=>'youtube_id','class'=>'form-control','placeholder'=>'youtube']) !!}	
				</div>
				<div class="form-group">
					{!! Form::label('post_type', 'Post type'); !!}											
					{!! Form::select(
						'post_type',
						[
							'standard'=>'Standard',
							'vocabulary'=> 'Vocabulary',
							'grammar'=> 'Grammar'
						],
						null,
						['class'=>'form-control','id'=>'post_type']
						) !!}	
				</div>
				<div class="form-group">
					{!! Form::label('thumbnail', 'Thumbnail'); !!}
					<div class="wrap-thumb">       
					   <input class="thumbUpload" type="file" name="post_thumbnail" />
					   <span id="thumbnail_old" class="hidden">{{ $post->post_thumbnail }}</span>
					   <div class="image-holder"></div>
					 </div>
				</div>		

				<input type="hidden" name="user_id" value="{{ Auth::user()->id }}">

				<div class="form-group text-right form-submit">			
					{!! Form::submit("Update",['class'=>'btn btn-primary','name'=>'save']) !!}
					<!-- {!! Form::submit("Save Draft",['class'=>'btn btn-default','name'=>'save_draft']) !!} -->
				</div>
			</div>									
		</div>

	{!! Form::close() !!}
@endsection