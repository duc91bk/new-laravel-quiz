@extends('backends.layouts.create')

@section('title','Edit youtube')

@section('body_class','create-youtube')

@section('entry_title', 'Sửa youtube')

@section('main_content')
	{!!
		Form::model($youtube, [
			'route'=>['youtube.update', $youtube->id],
			'method'=>'PUT',
			'class'=>'post_form'
		])
	!!}

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					{!! Form::label('youtube_title', 'Youtube title') !!}
					{!! Form::text('youtube_title',null,['id'=>'youtube_title','class'=>'form-control','placeholder'=>'Youtube title']) !!}	
				</div>
				<div class="form-group">
					{!! Form::label('youtube_url', 'Youtube url') !!}
					@if(!empty($youtube->youtube_code_id))
					<input type="text" id="youtube_url" name="youtube_url" class="form-control" placeholder="Youtube url" value="https://www.youtube.com/watch?v={{ $youtube->youtube_code_id }}">
					@else
					<input type="text" id="youtube_url" name="youtube_url" class="form-control" placeholder="Youtube url">
					@endif
				</div>
				<div class="form-group">
					{!! Form::label('youtube_en', 'Youtube En') !!}
					{!! Form::text('youtube_en',null,['id'=>'youtube_en','class'=>'form-control','placeholder'=>'Youtube En']) !!}	
				</div>
				<div class="form-group">
					{!! Form::label('youtube_vn', 'Youtube Vn') !!}
					{!! Form::text('youtube_vn',null,['id'=>'youtube_vn','class'=>'form-control','placeholder'=>'Youtube Vn']) !!}	
				</div>
				<div class="form-group text-right">
					{!! Form::button("Save",['class'=>'btn btn-primary','type'=>'submit']) !!}
				</div>		
			</div>
		</div>

	{!! Form::close() !!}    				
@endsection