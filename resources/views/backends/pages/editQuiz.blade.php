@extends('backends.layouts.create')

@section('title','Update Group Quiz')

@section('body_class','create-group-quiz')

@section('entry_title', 'Sửa câu hỏi trắc nghiệm')

@section('main_content')
	{!!
		Form::model($quiz, [
			'route'=>['quiz.update', $quiz->id],
			'method'=>'PUT',
			'class'=>'post_form',
			'files'=>true
		])
	!!}
		<div class="row">
			<div class="col-md-9">
				<div class="form-group">
					{!! Form::label('quiz_title', 'Title') !!}
					{!! Form::text('quiz_title',null,['id'=>'quiz_title','class'=>'form-control','placeholder'=>'Quiz title']) !!}	
				</div>
				<div class="form-group">
					{!! Form::label('post_content', 'Description'); !!}
					{!! Form::textarea('quiz_description',null,['id'=>'post_content','class'=>'form-control']) !!}
				</div>
			</div>
			<div class="col-md-3">				
				<div class="form-group">
					{!! Form::label('group_quiz_id', 'Group quiz id'); !!}
					{!! Form::text('group_quiz_id',null,['id'=>'group_quiz_id','class'=>'form-control','placeholder'=>'Group quiz id']) !!}					
				</div>
				<div class="form-group">
					{!! Form::label('quiz_type', 'Quiz type') !!}
					<input type="text" name="quiz_type" id="quiz_type" class="form-control" value="{{ $quiz->quiz_type }}" readonly>	
				</div>
				<div class="form-group">
					{!! Form::label('quiz_media', 'Audio'); !!}
					<input id="quiz_media" type="file" name="quiz_media" />
				</div>	
			</div>
		</div>
		
		@if($quiz->quiz_type == "select")
			<div class="row answers">
				<?php $count = 0; ?>
				@foreach($answers as $answer)
					<?php $count++; ?>
					<div class="col-md-3">
						<div class="form-group">
							<label for="answers_<?php echo ($count-1); ?>">Answer <?php echo $count; ?></label>
							<input type="text" name="answers_<?php echo ($count-1); ?>" class="form-control" value="{{ $answer->answer_title }}">
							<input type="hidden" name="id_answers_<?php echo ($count-1); ?>" value="{{ $answer->id }}">
						</div>				
					</div>		
				@endforeach
			</div>
		@endif

		<div class="row">
			<div class="col-md-3">
				<!-- select true answer -->
				@if($quiz->quiz_type == "select")
					<div class="form-group quiz_select">
						{!! Form::label('answer_is_correct', 'Câu trả lời đúng') !!}
						
						{!! Form::select(
								'answer_is_correct',
								[
									'0'=>'Answer 1',
									'1'=>'Answer 2',
									'2'=>'Answer 3',
									'3'=>'Answer 4'
								],
								null,
								[
									'id'=>'answer_is_correct',
									'class'=>'form-control'
								]
							) 
						!!}
					</div>
				@endif

				<!-- input true answer -->
				@if($quiz->quiz_type == "input")
					<div class="quiz_input">
						<?php $count = 0; ?>
						@foreach($answers as $answer)
							<?php $count++; ?>
							@if($count == 1)
								<div class="form-group quiz_input">												
									{!! Form::label('answer_text', 'Câu trả lời đúng') !!}
									<input type="text" name="answer_text[]" class="form-control" value="{{ $answer->answer_title }}">
									<input type="hidden" name="id_answers[]" value="{{ $answer->id }}">
								</div>
							@endif
						@endforeach

						<!-- input different true answer -->
						<div class="form-group">
							{!! Form::label('', 'Đáp án khác') !!}
							<div class="different-answers">
								<?php $count = 0; ?>
								@foreach($answers as $answer)
									<?php $count++; ?>
									@if($count > 1)
									<div class="form-group">											
										<input type="text" name="answer_text[]" class="form-control" value="{{ $answer->answer_title }}">
										<input type="hidden" name="id_answers[]" value="{{ $answer->id }}">
									</div>
									@endif
								@endforeach
							</div>
							<input type="hidden" name="num_answer">
							<a href="#" class="add_more btn btn-default">Add more</a>
						</div>
						<!-- form group -->
						
					</div>
				@endif

				
			</div>
			<div class="col-md-12">
				<div class="form-group text-right form-submit">
					{!! Form::submit("Save",['class'=>'btn btn-primary','name'=>'save']) !!}
					{!! Form::submit("Save Draft",['class'=>'btn btn-default','name'=>'save_draft']) !!}
				</div>
				<!-- form submit -->
			</div>
		</div>

	{!! Form::close() !!}    				
@endsection