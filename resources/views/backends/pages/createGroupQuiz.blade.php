@extends('backends.layouts.create')

@section('title','Create Group Quiz')

@section('body_class','create-group-quiz')

@section('entry_title', 'Tạo nhóm câu hỏi trắc nghiệm')

@section('main_content')
	{!!
		Form::open([
			'route'=>['group_quiz.store'],
			'method'=>'POST',
			'class'=>'post_form'
		])
	!!}

		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					{!! Form::label('group_quiz_title', 'Group quiz') !!}
					{!! Form::text('group_quiz_title',null,['id'=>'group_quiz_title','class'=>'form-control','placeholder'=>'Group quiz title']) !!}	
				</div>
				<div class="form-group text-right">
					{!! Form::button("Save",['class'=>'btn btn-primary','type'=>'submit']) !!}
				</div>		
			</div>
		</div>

	{!! Form::close() !!}    				
@endsection