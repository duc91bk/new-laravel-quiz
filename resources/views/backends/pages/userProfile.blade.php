@extends('backends.layouts.master')

@section('title', $user->name)

@section('body_class', 'page_user_profile')

@section('main_content')
	<div class="content-wrapper">
		<section class="content-header">
			<h1>
				Profile
			</h1>
        </section>
        <section class="content">
        	<div class="row">
        		<!-- <div class="col-md-12">
        			<div class="box box-info">
        				<div class="box-body">
							profile
							
        				</div>
        			</div>
        		</div> -->
        		<div class="col-md-3">
        			<div class="box box-info">
        				<div class="box-header"></div>
        				<div class="box-body">
        					<div class="text-center user-avatar">
	        					<?php
								$comment_author_email = $user->email;
								$gravatar_link = 'http://www.gravatar.com/avatar/' . md5($comment_author_email) . '?s=368';
								echo '<img src="' . $gravatar_link . '" class="img-responsive" />';
								?>
								<!-- <a href="{{ route('user.edit', Auth::user()->id)}}" class="btn btn-xs btn-warning edit"> <i class="fa fa-pencil append-icon"></i> Edit </a> -->
						  	</div>
						  	<h2>{{ $user->name }}</h2>
						  	<!-- <h4>Owner at Our Company, Inc.</h4> -->
						  	<!-- <p>{{ Auth::user()->description }}</p> -->
						  	<table class="table table-striped table-hover">
		                        <tbody>
		                          <!-- <tr>
		                            <td>User Rating</td>
		                            <td><i class="fa fa-star  fa-fw text-yellow"></i><i class="fa fa-star  fa-fw text-yellow"></i><i class="fa fa-star  fa-fw text-yellow"></i><i class="fa fa-star  fa-fw"></i><i class="fa fa-star  fa-fw"></i></td>
		                          </tr> -->
		                          <tr>
		                            <td>Tham gia từ:</td>
		                            <td>{{ $user->created_at->format('d-m-Y') }} </td>
		                          </tr>

		                          <tr>
		                          <?php
										$arNameLevel = array('Cấp 1', 'Cấp 2', 'Cấp 3', 'Sinh viên', 'Giáo viên', 'Admin', 'Super Admin');
									?>
		                            <td>Cấp bậc:</td>
		                            <td>{{ $arNameLevel[$user->user_level - 1] }}</td>
		                          </tr>
		                          <tr>
		                            <td>Điểm đạt được:</td>
		                            <td>{{ $user->user_score }} </td>
		                          </tr>
		                        </tbody>
		                    </table>
        				</div>
        			</div>
        			<!-- box info -->
        			<!-- <div class="box box-info">
        				<div class="box-header">
    						<h3 class="box-title"><i class="fa fa-users mgr-10"></i> STUDENTS</h3>
    					</div>
        				<div class="box-body">
        					<div class="row">
        						<div class="col-md-4 text-center mg-bt-12" >
        							<a href="#" class="dis-in"><img src="http://www.gravatar.com/avatar/4a6506c4ab9edc432579f848a816e0a7?s=84" alt="" class="img-circle img-responsive"></a>
        						</div>
        						<div class="col-md-4 text-center mg-bt-12">
        							<a href="#"  class="dis-in"><img src="http://www.gravatar.com/avatar/4a6506c4ab9edc432579f848a816e0a7?s=84" alt="" class="img-circle img-responsive"></a>
        						</div>
        						<div class="col-md-4 text-center mg-bt-12">
        							<a href="#"  class="dis-in"><img src="http://www.gravatar.com/avatar/4a6506c4ab9edc432579f848a816e0a7?s=84" alt="" class="img-circle img-responsive"></a>
        						</div>
        						<div class="col-md-4 text-center mg-bt-12" >
        							<a href="#" class="dis-in"><img src="http://www.gravatar.com/avatar/4a6506c4ab9edc432579f848a816e0a7?s=84" alt="" class="img-circle img-responsive"></a>
        						</div>
        						<div class="col-md-4 text-center mg-bt-12">
        							<a href="#"  class="dis-in"><img src="http://www.gravatar.com/avatar/4a6506c4ab9edc432579f848a816e0a7?s=84" alt="" class="img-circle img-responsive"></a>
        						</div>
        						<div class="col-md-4 text-center mg-bt-12">
        							<a href="#"  class="dis-in"><img src="http://www.gravatar.com/avatar/4a6506c4ab9edc432579f848a816e0a7?s=84" alt="" class="img-circle img-responsive"></a>
        						</div>
        						<div class="col-md-4 text-center mg-bt-12" >
        							<a href="#" class="dis-in"><img src="http://www.gravatar.com/avatar/4a6506c4ab9edc432579f848a816e0a7?s=84" alt="" class="img-circle img-responsive"></a>
        						</div>
        						<div class="col-md-4 text-center mg-bt-12">
        							<a href="#"  class="dis-in"><img src="http://www.gravatar.com/avatar/4a6506c4ab9edc432579f848a816e0a7?s=84" alt="" class="img-circle img-responsive"></a>
        						</div>
        						<div class="col-md-4 text-center mg-bt-12">
        							<a href="#"  class="dis-in"><img src="http://www.gravatar.com/avatar/4a6506c4ab9edc432579f848a816e0a7?s=84" alt="" class="img-circle img-responsive"></a>
        						</div>
        					</div>
        					<div class="closing text-center"> <a href="#">See All Friends<i class="fa fa-angle-double-right prepend-icon"></i></a> </div>
        				</div>
        			</div> -->
        		</div>
        		<!-- col 3 -->
        		<!-- <div class="col-md-9">
        			<div id="box-profile" class="box box-info box-solid">
        				<div class="box-header">
        					<h3 class="box-title">Profile</h2>
        				</div>
        				<div class="box-body">
        					<a href="{{ route('user.edit', Auth::user()->id)}}" class="btn btn-xs btn-warning edit"> <i class="fa fa-pencil append-icon"></i> Edit </a>
        					<h3 ><i class="fa fa-user"></i> ABOUT</h3>
        					<div class="row">
					          <div class="col-sm-6">
					            <div class="row">
					              <label class="col-xs-5 control-label">First Name:</label>
					              <div class="col-xs-7 controls">Mariah</div>
					            </div>
					          </div>
					          <div class="col-sm-6">
					            <div class="row">
					              <label class="col-xs-5 control-label">Last Name:</label>
					              <div class="col-xs-7 controls">Caraiban</div>
					            </div>
					          </div>
					          <div class="col-sm-6">
					            <div class="row">
					              <label class="col-xs-5 control-label">User Name:</label>
					              <div class="col-xs-7 controls">Mariah</div>
					            </div>
					          </div>
					          <div class="col-sm-6">
					            <div class="row">
					              <label class="col-xs-5 control-label">Email:</label>
					              <div class="col-xs-7 controls">mariah@Vendroid.com</div>
					            </div>
					          </div>
					          <div class="col-sm-6">
					            <div class="row">
					              <label class="col-xs-5 control-label">City:</label>
					              <div class="col-xs-7 controls">Los Angeles</div>
					            </div>
					          </div>
					          <div class="col-sm-6">
					            <div class="row">
					              <label class="col-xs-5 control-label">Country:</label>
					              <div class="col-xs-7 controls">United States</div>
					            </div>
					          </div>
					          <div class="col-sm-6">
					            <div class="row">
					              <label class="col-xs-5 control-label">Birthday:</label>
					              <div class="col-xs-7 controls">Jan 22, 1984</div>
					            </div>
					          </div>
					          <div class="col-sm-6">
					            <div class="row">
					              <label class="col-xs-5 control-label">Interests:</label>
					              <div class="col-xs-7 controls">Basketball, Web, Design, etc.</div>
					            </div>
					          </div>
					          <div class="col-sm-6">
					            <div class="row">
					              <label class="col-xs-5 control-label">Website:</label>
					              <div class="col-xs-7 controls"><a href="#">Vendroid.venmond.com</a></div>
					            </div>
					          </div>
					          <div class="col-sm-6">
					            <div class="row">
					              <label class="col-xs-5 control-label">Phone:</label>
					              <div class="col-xs-7 controls">+1-234-5678</div>
					            </div>
					          </div>
					        </div>
					        <hr>
					        <div class="row">
					          <div class="col-sm-7 ">
					            <h3><i class="fa fa-file-text-o "></i> EXPERIENCE</h3>
					            <div class="content-list content-menu">
					              <ul class="list-wrapper list-unstyled">
					                <li> <span class="menu-icon"><i class="fa  fa-circle-o text-aqua"></i></span> <span class="menu-text"> <a href="#">Owner</a> at <a href="#">Vendroid Ltd.</a> <span class="menu-info"><span class="menu-date"> March 2013 ~ Now</span></span> </span> </li>
					                <li> <span class="menu-icon"><i class=" fa  fa-circle-o text-aqua"></i></span> <span class="menu-text"> <a href="#">CEO</a> at <a href="#">Mc. Dondon</a> <span class="menu-info"><span class="menu-date"> March 2011 ~ February 2013</span></span> </span> </li>
					                <li> <span class="menu-icon"><i class=" fa  fa-circle-o text-aqua"></i></span> <span class="menu-text"> <a href="#">Web Designer</a> at <a href="#">Web Design Company Ltd.</a> <span class="menu-info"><span class="menu-date"> March 2010 ~ February 2011</span></span> </span> </li>
					                <li> <span class="menu-icon"><i class=" fa  fa-circle-o text-aqua"></i></span> <span class="menu-text"> <a href="#">Sales</a> at <a href="#">Sales Company Ltd.</a> <span class="menu-info"><span class="menu-date"> March 2009 ~ February 2010</span></span> </span> </li>
					              </ul>
					            </div>
					          </div>
					          <div class="col-sm-5">
					            <h3><i class="fa fa-trophy "></i> EDUCATION</h3>
					            <div class="content-list content-menu">
					              <ul class="list-wrapper list-unstyled">
					                <li> <span class="menu-icon"><i class="fa  fa-circle-o text-aqua"></i></span> <span class="menu-text"> Bachelor's degree, E-Commerce/Electronic Commerce at <a href="#">UCLA</a> <span class="menu-info"><span class="menu-date"> August 2003 ~ July 2008</span></span> </span> </li>
					                <li> <span class="menu-icon"><i class=" fa  fa-circle-o text-aqua"></i></span> <span class="menu-text"> Student at <a href="#">Web Design Education</a> <span class="menu-info"><span class="menu-date"> March 2006 ~ February 2007</span></span> </span> </li>                
					                <li> <span class="menu-icon"><i class=" fa  fa-circle-o text-aqua"></i></span> <span class="menu-text"> Student at <a href="#">St. Louis High School</a> <span class="menu-info"><span class="menu-date"> August 2000 ~ July 2003 </span></span> </span> </li>
					                <li> <span class="menu-icon"><i class=" fa  fa-circle-o text-aqua"></i></span> <span class="menu-text"> Student at <a href="#">St. Monica Junior High School</a> <span class="menu-info"><span class="menu-date"> August 1998 ~ July 2000</span></span> </span> </li>
					              </ul>
					            </div>            
					          </div>
					        </div>
					        <hr>
					        <div class="row">
					          <div class="col-sm-7">
					            <h3 class="mgbt-xs-15 font-semibold"><i class="fa fa-globe mgr-10 profile-icon"></i> ACTIVITY</h3>
					            <div class="">
					              <div class="content-list">
					                <div class="mCustomScrollbar" >
					                  <ul class="list-wrapper list-unstyled">
					                    <li> <span class="menu-icon "><i class="fa fa-suitcase"></i></span> <span class="menu-text"> Someone has give you a surprise <span class="menu-info"><span class="menu-date"> ~ 12 Minutes Ago</span></span> </span>  </li>
					                    <li> <span class="menu-icon"><i class=" fa fa-user"></i></span> <span class="menu-text"> Change your user profile details <span class="menu-info"><span class="menu-date"> ~ 1 Hour 20 Minutes Ago</span></span> </span> </li>
					                    <li> <span class="menu-icon"><i class=" fa fa-cogs"></i></span> <span class="menu-text"> Your setting is updated <span class="menu-info"><span class="menu-date"> ~ 12 Days Ago</span></span> </span></li>
					                    <li>  <span class="menu-icon"><i class=" fa fa-book"></i></span> <span class="menu-text"> Added new article <span class="menu-info"><span class="menu-date"> ~ 19 Days Ago</span></span> </span>  </li>
					                    <li>  <span class="menu-icon"><i class=" fa fa-user"></i></span> <span class="menu-text"> Change Profile Pic <span class="menu-info"><span class="menu-date"> ~ 20 Days Ago</span></span> </span> </li>
					                    <li>  <span class="menu-icon"><i class=" fa fa-cogs"></i></span> <span class="menu-text"> Your setting is updated <span class="menu-info"><span class="menu-date"> ~ 12 Days Ago</span></span> </span>  </li>
					                    <li>  <span class="menu-icon"><i class=" fa fa-book"></i></span> <span class="menu-text"> Added new article <span class="menu-info"><span class="menu-date"> ~ 19 Days Ago</span></span> </span> </li>
					                    <li>  <span class="menu-icon"><i class=" fa fa-user"></i></span> <span class="menu-text"> Change Profile Pic <span class="menu-info"><span class="menu-date"> ~ 20 Days Ago</span></span> </span>  </li>
					                  </ul>
					                </div>
					                <div class="closing text-center" style=""> <a href="#">See All Activities <i class="fa fa-angle-double-right"></i></a> </div>
					              </div>
					            </div>
					          </div>
					          <div class="col-sm-5">
					            <h3 class="mgbt-xs-15 font-semibold"><i class="fa fa-flask mgr-10 profile-icon"></i> SKILL</h3>
					            <div class="skill-list">
					              <div class="skill-name"> Photoshop </div>
					              <div class="progress  ">
					                <div style="width: 90%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="90" role="progressbar" class="progress-bar progress-bar-success "> <span class="sr-only">90%</span> </div>
					              </div>
					            </div>
					            <div class="skill-list">
					              <div class="skill-name"> Illustrator </div>
					              <div class="progress  ">
					                <div style="width: 20%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="20" role="progressbar" class="progress-bar progress-bar-danger "> <span class="sr-only">20%</span> </div>
					              </div>
					            </div>
					            <div class="skill-list">
					              <div class="skill-name"> PHP </div>
					              <div class="progress  ">
					                <div style="width: 50%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="50" role="progressbar" class="progress-bar progress-bar-warning "> <span class="sr-only">50% Complete</span> </div>
					              </div>
					            </div>
					            <div class="skill-list">
					              <div class="skill-name"> Javascript </div>
					              <div class="progress  ">
					                <div style="width: 60%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar" class="progress-bar progress-bar-info "> <span class="sr-only">60% Complete</span> </div>
					              </div>
					            </div>
					            <div class="skill-list">
					              <div class="skill-name"> Communication </div>
					              <div class="progress  ">
					                <div style="width: 95%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="95" role="progressbar" class="progress-bar progress-bar-success "> <span class="sr-only">95% Complete</span> </div>
					              </div>
					            </div> 
					            <div class="skill-list">
					              <div class="skill-name"> Writing </div>
					              <div class="progress  ">
					                <div style="width: 45%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="45" role="progressbar" class="progress-bar progress-bar-warning "> <span class="sr-only">45% Complete</span> </div>
					              </div>
					            </div>                                 
					          </div>
					        </div>
        				</div>
        			</div>
        		</div> -->
        	</div>
			
		</section>
		<!-- content -->

	</div>
	<!-- content-wrapper -->
@endsection