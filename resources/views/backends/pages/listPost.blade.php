@extends('backends.layouts.master')

@section('title', 'List post')

@section('body_class', 'page_list')

@section('main_content')
	<div class="content-wrapper">		
        <section class="content">
        	<div class="row">
        		<div class="col-md-12">
        			<div class="box box-primary box-list-item">
        				
		                <div class="box-header with-border">
							<h3 class="box-title">Danh sách các bài viết</h3>
							<div class="pull-right">
								<a href="{{ route('post.create') }}" class="btn btn-primary">Tạo bài mới</a>
							</div>
		                </div><!-- box-header -->
		                @if(count($posts)>0)
							<div class="clearfix">
								<div class="post-controls clearfix">
			                		<div class="box-tools pull-right">
										<div class="has-feedback">
											{{-- <span class="glyphicon glyphicon-search form-control-feedback"></span> --}}
											{!!
												Form::open([
													'route'=>['dashboard.search.post'],
													'method'=>'GET',
													'class'=>'search_form'
												])
											!!}
												{!! Form::text('key',null,['id'=>'key','class'=>'form-control','placeholder'=>'Enter your search term...']) !!}					
												{!! Form::button("",['class'=>'btn btn-default fa fa-search form-control-feedback','type'=>'submit']) !!}
											{!! Form::close() !!}
										</div>
									</div><!-- box-tools -->
			                	</div>
		                	</div>
						@endif
		                @if(count($posts)>0)
			                {!!
								Form::open([
									'route'=>['posts.destroy_list_items'],
									'method'=>'DELETE',
									'class'=>'post_form'
								])
							!!}
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
			                <div class="box-body no-padding">
			                	
			                  <div class="post-controls clearfix">
			                  	
			                    

			                    <div class="pull-right">
			                      <div class="posts-count pull-left">
			                      	{{ $posts->total() }} items
			                      </div>
			                      <div class="pull-left">
			                        {!! $posts->links() !!}
			                      </div><!-- btn-group -->
			                    </div><!-- pull-right -->
			                  </div>
			                  <div class="table-responsive ">
			                    <table class="table table-hover table-striped">
									<thead>
										<tr>
											<th class="post-user">User</th>
											<th class="post-cat">Category</th>
											<th class="post-title" colspan="3">Title</th>
											<th class="post-type">Type</th>										
											<th class="post-date">Created at</th>
											<th class="post-date">Updated at</th>
										</tr>
									</thead>
			                      	<tbody>
				                        @foreach($posts as $post)				                        	
											<tr>
					                          	<td class="post-user" width="150"><a href="/"> {{ $post->name }} </a></td>
					                          	<td class="post-cat" width="150"><a href="/">{{ $post->cat_title }}</a></td>
					                          	<td class="post-title"><a href="{{ route('post.show', $post->id)}}">{{ $post->post_title }}</a></td>

					                          	<td class="post-edit" width="50">				
					                          		@if(Auth::user()->id == $post->user_id || ( Auth::user()->user_level > $post->user_level  && Auth::user()->user_level > 3 ))                          		
					                          		<a href="{{ route('post.edit', $post->id)}}">Edit</a>
					                          		@endif
					                          	</td>
					                          	<td class="post-delete" width="60">
					                          		@if(Auth::user()->id == $post->user_id || ( Auth::user()->user_level > $post->user_level  && Auth::user()->user_level > 3 ))                          		
					                          		<a href="{{ route('post.destroy', $post->id)}}" class="text-danger">Delete</a>
					                          		@endif
					                          	</td>
					                          	<td class="post-type" width="80">{{ $post->post_type }}</td>		
					                          	<td class="post-date" width="140">{{ $post->created_at }}</td>
					                          	<td class="post-date" width="140">{{ $post->updated_at }}</td>
					                        </tr>					                        
										@endforeach
			                      	</tbody>
			                    </table><!-- table -->
			                  </div><!-- mail-box-messages -->
			                </div><!-- box-body -->
			                <div class="box-footer no-padding">
			                  <div class="post-controls clearfix">
			                    <div class="pull-right">
			                      	<div class="posts-count pull-left">
				                    	@if($posts->total() == 1)
				                      		{{ $posts->total() }} item
				                      	@else 
				                      		{{ $posts->total() }} item
				                      	@endif
			                      	</div>
			                      	<div class="pull-left">
			                        	{!! $posts->links() !!}
			                      	</div><!-- btn-group -->
			                    </div><!-- pull-right -->
			                  </div>
			                </div>
							
			                <input type="hidden" name="destroy_list_items" value="">			                

			                {!! Form::close() !!}
		                @else 
							<p>Bạn chưa có bài viết nào.</p>
		                @endif
		            </div><!-- box -->
        		</div>
        	</div>
			
		</section>
		<!-- content -->

	</div>
	<!-- content-wrapper -->
@endsection