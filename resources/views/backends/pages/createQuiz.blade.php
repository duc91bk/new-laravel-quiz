@extends('backends.layouts.create')

@section('title','Create Quiz')

@section('body_class','create-quiz')

@section('entry_title', 'Tạo câu hỏi mới')

@section('main_content')
	{!!
		Form::open([
			'route'=>['quiz.store'],
			'method'=>'POST',
			'class'=>'post_form',
			'files'=>true
		])
	!!}
		<div class="row">			
			<div class="col-md-9">
				<div class="form-group">
					{!! Form::label('quiz_title', 'Title') !!}
					{!! Form::text('quiz_title',null,['id'=>'quiz_title','class'=>'form-control','placeholder'=>'Quiz title']) !!}	
				</div>
				<div class="form-group">
					{!! Form::label('post_content', 'Description'); !!}
					{!! Form::textarea('quiz_description',null,['id'=>'post_content','class'=>'form-control']) !!}
				</div>
			</div>
			<div class="col-md-3">				
				<div class="form-group">
					{!! Form::label('group_quiz_id', 'Group quiz id'); !!}
					{!! Form::text('group_quiz_id',null,['id'=>'group_quiz_id','class'=>'form-control','placeholder'=>'Group quiz id']) !!}					
				</div>
				<div class="form-group">
					{!! Form::label('quiz_type', 'Quiz type') !!}
					{!! Form::select('quiz_type',['select'=>'select','input'=>'input'],null,['id'=>'quiz_type','class'=>'form-control']) !!}	
				</div>
				<div class="form-group">
					{!! Form::label('quiz_media', 'Audio'); !!}
					<input id="quiz_media" type="file" name="quiz_media" />
				</div>	
			</div>
		</div>

		<div class="row answers">
			<div class="col-md-3">
				<div class="form-group">
					{!! Form::label('answer_1', 'Answer 1') !!}
					{!! Form::text('answers_0',null,['id'=>'answer_1','class'=>'form-control','placeholder'=>'Answer 1']) !!}
					<div class="wrap-thumb">       
					   {!! Form::file('thumbnail0', ['class'=>'thumbUpload']) !!}
					   <div class="image-holder"> </div>
					</div>
				</div>
				<!-- end answer 1 -->
			</div>
			<div class="col-md-3">
				<div class="form-group">
					{!! Form::label('answer_2', 'Answer 2') !!}
					{!! Form::text('answers_1',null,['id'=>'answer_2','class'=>'form-control','placeholder'=>'Answer 2']) !!}
					<div class="wrap-thumb">       
					   <input class="thumbUpload" type="file" name="thumbnail1" />
					   <div class="image-holder"> </div>
					</div>
				</div>
				<!-- end answer 2 -->
			</div>
			<div class="col-md-3">
				<div class="form-group">
					{!! Form::label('answer_3', 'Answer 3') !!}
					{!! Form::text('answers_2',null,['id'=>'answer_3','class'=>'form-control','placeholder'=>'Answer 3']) !!}
					<div class="wrap-thumb">       
					   <input class="thumbUpload" type="file" name="thumbnail2" />
					   <div class="image-holder"> </div>
					</div>
				</div>
				<!-- end answer 3 -->
			</div>
			<div class="col-md-3">
				<div class="form-group">
					{!! Form::label('answer_4', 'Answer 4') !!}
					{!! Form::text('answers_3',null,['id'=>'answer_4','class'=>'form-control','placeholder'=>'Answer 4']) !!}	
					<div class="wrap-thumb">       
					   <input class="thumbUpload" type="file" name="thumbnail3" />
					   <div class="image-holder"> </div>
					</div>
				</div>
				<!-- end answer 4 -->
			</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="form-group quiz_select show">
					{!! Form::label('is_correct', 'Câu trả lời đúng') !!}
					{!! Form::select(
							'answer_is_correct',
							[
								'0'=>'Answer 1',
								'1'=>'Answer 2',
								'2'=>'Answer 3',
								'3'=>'Answer 4'
							],
							null,
							[
								'id'=>'answer_is_correct',
								'class'=>'form-control'
							]
						) 
					!!}
				</div>
				<div class="quiz_input hidden">
					<div class="form-group">
						{!! Form::label('answer_text_1', 'Câu trả lời đúng') !!}
						{!! Form::text('answer_text_0',null,['id'=>'answer_text_1', 'class'=>'form-control','placeholder'=>'Nhập câu trả lời đúng vào đây']) !!}
					</div>

					<div class="form-group">
						{!! Form::label('', 'Đáp án khác') !!}
						<div class="different-answers">
							<div class="form-group">											
								{!! Form::text('answer_text_1',null,['class'=>'form-control','placeholder'=>'Nhập câu trả lời đúng khác vào đây']) !!}
							</div>
						</div>
						<input type="hidden" name="num_answer">
						<a href="#" class="add_more btn btn-default">Add more</a>
					</div>
				</div>

				
			</div>
			<div class="col-md-12">
				<div class="form-group text-right form-submit">
					{!! Form::submit("Save",['class'=>'btn btn-primary','name'=>'save']) !!}
				</div>
			</div>
		</div>

	{!! Form::close() !!}    				
@endsection