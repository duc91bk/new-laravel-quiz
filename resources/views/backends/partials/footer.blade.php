	<!-- Main Footer -->
	<footer class="main-footer">
	  <!-- To the right -->
	  <div class="pull-right hidden-xs">
	    Anything you want
	  </div>
	  <!-- Default to the left -->
	  <strong>Copyright &copy; 2015 <a href="#">Company</a>.</strong> All rights reserved.
	</footer>
	</div><!-- ./wrapper -->

	<script src="/backends/plugins/jQuery/jQuery.min.js"></script>
    <script src="/backends/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="/backends/plugins/adminLte/dist/js/app.min.js"></script>
    <script src="/backends/plugins/tinymce/tinymce.min.js"></script>
    <script src="/backends/js/admin.js"></script>
  </body>
</html>