<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">

    <!-- Sidebar user panel (optional) -->
    <div class="user-panel">
      <div class="pull-left image">
        <?php
          $comment_author_email=Auth::user()->email;
          $gravatar_link = 'http://www.gravatar.com/avatar/' . md5($comment_author_email) . '?s=45';
          echo '<img src="' . $gravatar_link . '" class="img-circle" />';
        ?>
      </div>
      <div class="pull-left info">
        <p>{{Auth::user()->name}}</p>
        <!-- Status -->
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>

    <!-- search form (Optional) -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search..." />
        <span class="input-group-btn">
          <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
        </span>
      </div>
    </form>
    <!-- /.search form -->
    <!-- Sidebar Menu -->
    {!! $DashboardSidebarMenu->asUl(array('class' => 'sidebar-menu')) !!}
  </section>
  <!-- /.sidebar -->
</aside>