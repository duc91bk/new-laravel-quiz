<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="/backends/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/backends/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/backends/plugins/adminLte/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="/backends/plugins/adminLte/dist/css/skins/skin-blue.min.css">
    <link rel="stylesheet" href="/backends/css/admin.css">
  </head>

  <body class="skin-blue sidebar-mini @yield('body_class')">

    @section('main_content')
    @show

    <script src="/backends/js/libs.js"></script>
    <script src="/backends/js/main.js"></script>    
  </body>
</html>
