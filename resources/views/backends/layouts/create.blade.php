@include('backends.partials.header')

@include('backends.partials.sidebar')


<div class="content-wrapper">
    <section class="content-header">
        <h1 class="entry-title">@yield('entry_title')</h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-body">
                        @include ('errors.form')

                        @section('main_content')

                        @show                            
                    </div>
                </div>
            </div>
        </div>          
    </section>
    <!-- content -->

</div>
<!-- content-wrapper -->


@include('backends.partials.controlSidebar')

@include('backends.partials.footer')

    