<div class="col-md-4">
	<div class="list-item" data-id="{{ $post->id }}" data-type="{{$post->post_type}}">
		<a href="{{ route('post.show', $post->id)}}" class="list-item-thumbnail">
		@if (!empty($post->post_thumbnail))			
			<img src="{{ $post->post_thumbnail }}" alt="{{ $post->post_title }}">
		@else
			<img src="/frontends/images/default/320x180.png" alt="{{ $post->post_title }}">			
		@endif
		@if(!empty($post->youtube_id))
			<i class="fa fa-play icon-play"></i>
		@endif
		</a>
		<h3 class="list-item-title"><a href="{{ route('post.show', $post->id)}}" title="{{ $post->post_title }}">{{ $post->post_title }}</a></h3>				
	</div>
</div>
<!-- col -->
