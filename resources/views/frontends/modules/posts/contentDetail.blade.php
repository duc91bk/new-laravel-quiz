<article class="post post-{{ $post->id }}">
    @if(!empty($youtube))
        <div class="media-item">
            <div class="youtube_content embed-responsive embed-responsive-16by9">
                <iframe src="https://www.youtube.com/embed/{{ $youtube->youtube_code_id }}" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
        <!-- media item -->
    @endif
    <div class="entry-header">
        <h1 class="entry-title">{{ $post->post_title }}</h1>
        <div class="metadata">
            <span class="metadata-author"><i class="fa fa-user"></i><a href="{{ route('post.listOfUser', $user->id) }}"> {{ $user->name }}</a></span>
            <span class="metadata-view"><i class="fa fa-eye"></i>{{ $post->post_view_count }}</span>
            <span class="metadata-time"><i class="fa fa-calendar"></i> {{ $post->created_at->format('d-m-Y') }}</span>
        </div>
    </div>
    <!-- entry header -->
    
    @if(!empty($post->post_content))
    <div class="entry-content">                     
        {!! $post->post_content !!}
    </div>
    @endif
</article>
<!-- post -->