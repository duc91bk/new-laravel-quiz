<div class="panel panel-default sub-item">
    <div class="panel-heading" role="tab" id="headingOne">
        <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
        Transcripts
        </a>
        </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in"  aria-labelledby="headingOne">
        <div class="panel-body">
            <div class="panel-group" id="accordion2"  aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading"  id="headingOne">
                        <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion2" href="#collapseOne2" aria-expanded="true" aria-controls="collapseOne" >
                        Enghlist Sub
                        </a>
                        </h4>
                    </div>
                    <div id="collapseOne2" class="panel-collapse collapse in"  aria-labelledby="headingOne">
                        <div class="panel-body">
                            {!! $youtube->youtube_en !!}
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading"  id="headingTwo">
                        <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion2" href="#collapseOne3" aria-expanded="true" aria-controls="collapseTwo" class="collapsed">
                        Viet Sub
                        </a>
                        </h4>
                    </div>
                    <div id="collapseOne3" class="panel-collapse collapse"  aria-labelledby="headingTwo">
                        <div class="panel-body">
                            {!! $youtube->youtube_vn !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>