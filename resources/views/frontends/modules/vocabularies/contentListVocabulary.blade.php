  
<ul class="list-group list-vocabulary">
@foreach($vocabularies as $vocabulary)
    <li class="list-group-item clearfix">
        <div class="list-group-item-thumbnail pull-left">
            <img src="{{ $vocabulary->vocabulary_thumbnail }}" alt="">
        </div>
        <div class="list-group-item-content">
            <div>{{ $vocabulary->vocabulary_title }} {{ $vocabulary->vocabulary_ipa }}</div>
            <audio controls>
                <source src="{{ $vocabulary->vocabulary_media }}" type="audio/mpeg">
                Trình duyệt của bạn không hỗ trợ audio    
            </audio>
            {!! $vocabulary->vocabulary_content !!}
        </div>
    </li>
@endforeach
</ul>