<article class="post post-{{ $vocabulary->id }}">
    
    <div class="entry-header">
        <h1 class="entry-title">{{ $vocabulary->vocabulary_title }}</h1>
    </div>
    <!-- entry header -->
    
    <div class="entry-content">                     
        <div class="post-audio">
            <audio controls>
                <source src="{{ $vocabulary->vocabulary_media }}" type="audio/mpeg">
                Trình duyệt của bạn không hỗ trợ audio    
            </audio>
        </div>
        {!! $vocabulary->vocabulary_content !!}
    </div>
</article>
<!-- post -->