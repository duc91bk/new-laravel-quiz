<header id="page-header">		
	<div class="page-header-middle">
		<div class="container">
			<a id="logo-top" href="{{ url('/') }}">
				<img src="/frontends/images/logo.png" alt="lang4u">
			</a>
		</div>
	</div>
	<!-- header middel -->
		
	<div class="page-header-bottom">
		<div class="container">				

			{!! $FrontendTopMenu->asUl(array('class' => 'sf-menu', 'id' => 'primary-menu')) !!}

			<div class="search-box pull-right">
				{!!
					Form::open([
						'route'=>['search'],
						'method'=>'GET',
						'class'=>'search_form'
					])
				!!}
					{!! Form::text('searchKey',null,['id'=>'search','class'=>'form-control','placeholder'=>'Tìm kiếm']) !!}					
					{!! Form::button("",['class'=>'btn btn-default fa fa-search','type'=>'submit']) !!}
				{!! Form::close() !!}
			</div>
		</div>
	</div>
	<!-- header bottom -->		
</header>