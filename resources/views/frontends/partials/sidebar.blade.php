<div class="widget widget-user">
	<div class="widget-content">
		@if(Auth::check())
			<div class="info-user media">
				  <div class="media-left user-avatar">
				    <a href="#">
				    	<?php 
							$gravatar_link = 'http://www.gravatar.com/avatar/' . md5(Auth::user()->email) . '?s=80';
						?>
				      <img src="{{ $gravatar_link }}" alt="">
				    </a>
				  </div>
				  <div class="media-body">
				    <ul class="list-unstyled">
						<li class="user-name">{{ Auth::user()->name}}</li>
						<?php
							$arNameLevel = array('Cấp 1', 'Cấp 2', 'Cấp 3', 'Sinh viên', 'Giáo viên', 'Admin', 'Super Admin');
						?>
						<li class="user-score">Cấp bậc: {{ $arNameLevel[Auth::user()->user_level - 1] }}</li>
						<li><a href="{{ route('user.profile', Auth::user()->id) }}">Tài khoản</a></li>
						<li><a href="{{ url('/logout') }}">Đăng xuất</a></li>	
					</ul>
				  </div>
			</div>
			
		@else
			<p>Hãy đăng nhập để có thể sử dụng các chức năng rèn luyện giúp bạn tăng kĩ năng ngoại ngữ của mình.</p>
			<a class="btn btn-primary" href="{{ url('/login') }}">Đăng nhập</a>
		@endif
	</div>
</div>
<!-- widget user -->

@if(Auth::check())
<div class="widget widget-practive">
	@if($currentNameRoute == "post.show")
	<p>Học một ngôn ngữ đòi hỏi một quá trình luyện tập hàng ngày vì vậy hãy chăm chỉ luyện tập một cách thường xuyên nhất.</p>
	<button class="btn btn-primary practive-single">Luyện tập</button>
	@else
	<p>Học một ngôn ngữ đòi hỏi một quá trình luyện tập hàng ngày vì vậy hãy chăm chỉ luyện tập một cách thường xuyên nhất.</p>
	<button class="btn btn-primary practive-all" >Luyện tập tổng quát</button>
	@endif
</div>
<!-- widet practive -->
@endif

@if(Auth::check() && count($postsHistoryPost) > 0)
<div class="widget widget-history-post">
	<h3 class="widget-title">Các bài đã xem</h3>
	<div class="widget-content">
		<ul class="list-group">
			<?php $postsHistoryPost = array_reverse($postsHistoryPost); ?>
			@for($i=0; $i < count($postsHistoryPost); $i++)
				@if($i < 5) 
					<li class="list-group-item" ><a href="{{ route('post.show', $postsHistoryPost[$i]['id'])}}">{{ $postsHistoryPost[$i]['post_title'] }}</a></li>	
				@endif
			@endfor			
		</ul>
		
	</div>
</div>
<!-- widget history post -->
@endif

<div class="widget widget-order-person">
	<h3 class="widget-title">Bảng xếp hạng</h3>
	<div class="widget-content">
		<table class="table table-bordered">
			@foreach ($users as $user)
				<tr>
					<td>{{ $user->name }}</td>
					<td>{{ $user->user_score }}</td>
				</tr>
			@endforeach
		</table>
	</div>
</div>
<!-- widget-order-person -->
