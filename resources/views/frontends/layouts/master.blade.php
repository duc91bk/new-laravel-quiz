<!DOCTYPE html>
<html >
<head>
    <meta charset="UTF-8">
    <title> @yield('title') </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="/frontends/css/main.css">
    <link rel="stylesheet" href="/frontends/css/jquery-ui.css">    
    <script src="/frontends/js/angular.min.js"></script>
</head>
<body class="@yield('body_class')" ng-app="myApp" ng-controller="myCtrl">
	@include('frontends.partials.header')
	<div id="main-content" class="container">
		<div class="row">
			<div id="main-section" class="col-md-9">
				@section('main_section')
				@show			
			</div>
			<div id="sidebar" class="col-md-3">
				@section('sidebar')
					@include('frontends.partials.sidebar')
				@show			
			</div>
		</div>
	</div>
	
	@include('frontends.partials.footer')
	@if(Auth::check())
    <div class="modal fade" id="modalPostQuiz" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" ng-controller="detaiPostQuiz">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel"></h4>
            <div class="progress">
              <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                <!-- <span >40% Complete (success)</span> -->
              </div>
            </div>
          </div>
          <div class="modal-body">

          </div>
          <div class="modal-footer">
            <div class="result pull-left text-left">
                <span class="result-status hide"></span>
                <h4 class="result-true-label hide">Đáp án đúng</h4>
                <p class="result-true"></p>
            </div>
            <button type="button" class="btn btn-default" >Bỏ qua</button>
            <button type="button" class="btn btn-primary check">Kiểm tra</button>
          </div>
        </div>
      </div>
    </div>
    @endif
    <style>
    .modal-dialog {
        width: 870px;
    }
    .result-true {
        margin: 0;
    }
    .modal-title {
        margin-bottom: 10px;
    }
    #modalPostQuiz .progress-bar' {
        margin-bottom: 0;
    }    
    #result-true-label {
        margin-top: 0;
    }
    .detail_page .list-vocabulary {
        background: #fff;
        border-radius: 4px;
    }
    .ui-autocomplete {
        width: 250px !important;
        max-height: 400px;
        overflow-y: scroll;
    }
    .ui-autocomplete .list-group-item {
        padding: 6px 12px;
    }
    #sidebar .widget-content {
        background: #fff;
    }
    </style>
	@section('after_footer')
	@show


	<script src="/frontends/js/libs.js"></script>
	<script src="/frontends/js/jquery-ui.min.js"></script>
	<script src="/frontends/js/functions.js"></script>
	<script src="/frontends/js/main.js"></script>
	@section('after_footer_script')
	@show
</body>
</html>