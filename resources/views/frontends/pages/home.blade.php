@extends('frontends.layouts.master')

@if($currentNameRoute == 'home')
@section('title', 'Home page')
@section('body_class', 'home_page')
@else
@section('title', 'List page')
@section('body_class', 'list_page')
@endif



@section('main_section')
	@if(count($posts) > 0)
		<div class="widget widget-list-items" data-type="{{ $postType }}"  data-user-id="{{$userId}}">
			<div class="row">
				@foreach($posts as $post)
					@include('frontends.modules.posts.contentList')	
				@endforeach
			</div>
		</div>  
		<div class="text-center">
			@if(count($posts) > 3)
			<a href="#" class="btn btn-default" id="load-more"><i class="fa"></i>Tải thêm</a>	
			@endif
		</div>
	@else
		<p>Chưa có bài viết nào</p>
	@endif
@endsection
