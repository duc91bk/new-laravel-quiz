@extends('frontends.layouts.master')

@section('title', 'Home page')

@section('body_class', 'home_page')

@section('main_section') 
    <section class="wrap-item">        
        @include('frontends.modules.vocabularies.contentVocabulary')
    </section>           
@endsection