@extends('frontends.layouts.master')

@section('title', $post->post_title)

@section('body_class', 'detail_page')

@section('main_section') 
    <section class="wrap-item">        
        @include('frontends.modules.posts.contentDetail')

        @if(!empty($youtube->youtube_en) || !empty($youtube->youtube_vn))
        <section class="wrap-item">                        
            <div class="panel-group" id="accordion"  aria-multiselectable="true">
                @include('frontends.modules.transcripts.transcript')
            </div>
        </section>  
        @endif      
        @if($post->post_type == 'vocabulary')
            @include('frontends.modules.vocabularies.contentListVocabulary')
        @endif
        <input type="hidden" id="group_quiz_id" value="{{$post->group_quiz_id}}">
    </section>           
@endsection
