<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    //protected $table = 'posts'; mac dinh Post model duoc dung cho posts table, Nhung neu dung table khac cho Post model thi khai bao them thuoc tinh nay

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'cat_id', 'group_quiz_id', 'post_title', 'post_thumbnail', 'post_content', 'post_ensub', 'post_vnsub', 'post_type', 'post_youtube', 'post_view_count'
    ];

    /**
     * Get the group quiz that owns the article.
     */
    public function getGroupQuiz()
    {
        return $this->belongsTo('App\GroupQuiz','group_quiz_id','id');
    }

    /**
     * Get the user that owns the article.
     */
    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }

    public function vocabularies()
    {
        return $this->hasMany('App\Vocabulary','post_id','id');
    }

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    /**
     * Get the article's title.
     *
     * @param  string  $value
     * @return string
     */
    
    public function getTitleAttribute($value)
    {
        return $this->attributes['title'] = ucfirst(trim($value));
    }
    public function getYoutube()
    {
        return $this->belongsTo('App\Youtube', 'youtube_id', 'id');
    }

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];
}
