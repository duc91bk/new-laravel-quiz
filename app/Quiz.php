<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'quiz_title', 'quiz_type', 'group_quiz_id'
    ];

    /**
     * Get the quizzes for the group quiz.
     */
    public function answers()
    {
        return $this->hasMany('App\Answer','quiz_id','id');
    }

    /**
     * Get the group quiz that owns the article.
     */
    public function group_quizzes()
    {
        return $this->belongsTo('App\GroupQuiz','group_quiz_id','id');
    }

    public function post()
    {
        return $this->belongsTo('App\Post','post_id','id');
    }

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];
}
