<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Post;
use App\GroupQuiz;
use App\Quiz;
use App\Cat;
use App\Youtube;
use Auth;
use App\User;
use Image;
use Carbon\Carbon;
// use Route;

class PostsController extends Controller
{
    public function index()
    {
        $posts = Post
            ::orderBy('id','desc')
            ->where('posts.post_type', '=', 'standard')
            ->select('posts.*')
            ->take(24)
            ->get();
        $postType = 'standard';
        $userId = '';

        return view('frontends.pages.home', compact('posts', 'postType', 'userId'));
    }

    public function listOfType($postType, Request $request)
    {
       
        $posts = Post
            ::orderBy('id','desc')
            ->where('posts.post_type', '=', $postType)
            ->select('posts.*')
            ->take(24)
            ->get();
        $userId = '';
        
        return view('frontends.pages.home', compact('posts', 'postType', 'userId'));
    }

    public function listOfCat($catId, Request $request)
    {
       
        $posts = Post
            ::orderBy('id','desc')
            ->where('posts.cat_id', '=', $catId)
            ->select('posts.*')
            ->take(24)
            ->get();
            
        $postType = '';
        return view('frontends.pages.home', compact('posts', 'catId', 'postType'));
    }

    public function ajaxIndex(Request $request){
        $lastPostId = $request->lastPostId;
        $postType = $request->postType;
        $userId = $request->userId;

        if (empty($postType) && !empty($userId)) {
            $posts = Post
                    ::orderBy('id','desc')
                    ->where('posts.id', '<', $lastPostId)
                    ->where('posts.user_id', '=', $userId)
                    ->select('posts.*')
                    ->take(9)
                    ->get();   
        } elseif(!empty($postType)) {
            $posts = Post
            ::orderBy('id','desc')
            ->where('posts.id', '<', $lastPostId)
            ->where('posts.post_type', '=', $postType)
            ->select('posts.*')
            ->take(9)
            ->get();
        }

        return response()->json($posts);
    }

    public function listOfUser($userId)
    {        
        $posts = Post
            ::orderBy('id','desc')
            ->where('user_id', '=', $userId)
            ->select('posts.*')
            ->take(24)
            ->get();
        $postType = '';

        return view('frontends.pages.home', compact('posts', 'userId', 'postType'));
    }

    public function listPost()
    {
        $user_id = Auth::user()->id;
        $posts = Post
            ::orderBy('id','desc')
            ->join('cats', 'posts.cat_id', '=', 'cats.id')
            ->join('users', 'posts.user_id', '=', 'users.id')
            ->select('posts.*','users.name','users.user_level','cats.cat_title')
            ->paginate(20);

        return view('backends.pages.listPost', compact('posts'));
    }

    public function create()
    {
        $cats = Cat::all();
        $arCats = array();
        foreach ($cats as $cat) {
            $arCats[$cat->id] = $cat->cat_title;
        }

        return view('backends.pages.createPost', compact('arCats'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'post_title' => 'required|unique:posts|min:3|max:255',
        ]);

        $post = new Post;

        $post->post_title = $request->post_title;

        if ($request -> has('post_content')) {
            $post->post_content = $request->post_content;
        }

        if ( $request->has('youtube_id') ) {
            $youtubeId = $request->youtube_id;
            $youtube = Youtube::find($youtubeId);
            $post->youtube_id = $request->youtube_id;
            $post->post_thumbnail = $youtube->youtube_thumbnail;
        }

        if ($request->has('group_quiz_id')) {
            $post->group_quiz_id = $request->group_quiz_id;
        }
        
        if ($request->has('cat_id')) {
            $post->cat_id = $request->cat_id;    
        }
        
        $post->post_type = $request->post_type;
             

        if ($request->hasFile('post_thumbnail') && $request->file('post_thumbnail')->isValid()) {
            $post->post_thumbnail = $this->getThumbnail($request);
        }
       
        $post->user_id = Auth::user()->id;     

        // $request->has("save_draft") ? $post->active = 0 : $post->active = 1;

        $post->save();
        $request->session()->flash('status_action_post', 'Bài viết được tạo thành công');

        return redirect()->route('post.edit', $post->id);
    }

    public function show($id, Request $request)
    {      
        $post = Post::find($id);
        $user = $post->user;

        if (Auth::check()) {
            $userCurrent = User::find(Auth::user()->id);
            $oldUserHistory = explode(',', $userCurrent->user_history);
            $idHistory = "post_" . $id;
            if (!in_array("post_" . $id, $oldUserHistory)) {
                array_push($oldUserHistory, $idHistory);
            } else {
                for ($j=0; $j < count($oldUserHistory); $j++) { 
                    if ($oldUserHistory[$j] == $idHistory) {
                        unset($oldUserHistory[$j]);
                        array_push($oldUserHistory, $idHistory);
                    }
                }
            }
            
            $oldUserHistory = implode(",", $oldUserHistory);
            $userCurrent->user_history = $oldUserHistory;
            $userCurrent->save();
        }
        

        $timeViewPage = "post_" . $id . "timeViewPage";
        if ($request->session()->has($timeViewPage)) {            
            $oldTime = trim($request->session()->get('timeViewPage'), "post_" . $id . "timeViewPage" . "_");
            $newIime = Carbon::now()->timestamp / 60;
            if (($newIime - $oldTime) > 3) {
                $post->post_view_count += 1;    
                $request->session()->put($timeViewPage, $timeViewPage . "_" . $newIime);
                $post->save();
            }
        } else {
            $newIime = Carbon::now()->timestamp / 60;
            $request->session()->put($timeViewPage, $timeViewPage . "_" . $newIime);
            $post->post_view_count += 1;
            $post->save();
        }
        
        if (!empty($post->youtube_id)) {
            $youtube = $post->getYoutube;
        } else {
            $youtube = '';
        }

        if ($post->post_type == 'vocabulary') {

            $vocabularies = $post->vocabularies;
            // var_dump($vocabularies);
            // dd('die');
            return view('frontends.pages.detail', compact('post', 'user', 'youtube', 'vocabularies'));   
        } else {
            return view('frontends.pages.detail', compact('post', 'user', 'youtube'));    
        }
              
    }

    public function edit($id, Request $request)
    {
        $post = Post::find($id);
        $user = $post->user;

        if ($post->user_id == Auth::user()->id || Auth::user()->user_level > $user->user_level) {
            $cats = Cat::all();
            $arCats = array();
            foreach ($cats as $cat) {
                $arCats[$cat->id] = $cat->cat_title;
            }
            $groupQuizzes = GroupQuiz::all();    
            if ($request->session()->has('status_action_post')) {
                $status_action_post = $request->session()->get('status_action_post');
                return view('backends.pages.editPost', compact('post', 'arCats', 'status_action_post', 'groupQuizzes'));
            } else {
                return view('backends.pages.editPost', compact('post', 'arCats', 'groupQuizzes'));    
            }            
        } else {
            return abort(401);
        }
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'post_title' => 'required|min:3|max:255',
        ]);
        $post = Post::find($id);   
        $user = $post->user;
        if ($post->user_id == Auth::user()->id || Auth::user()->user_level > $user->user_level) {            
            
            $post->post_title = $request->post_title;

            if ($request -> has('post_content')) {
                $post->post_content = $request->post_content;
            }

            if ( $request->has('youtube_id') ) {
                $youtubeId = $request->youtube_id;
                $youtube = Youtube::find($youtubeId);
                $post->youtube_id = $request->youtube_id;
            } 

            if ($request->has('group_quiz_id')){
                $post->group_quiz_id = $request->group_quiz_id;
            }            

            $post->cat_id = $request->cat_id;

            $post->post_type = $request->post_type;

            if ($request->hasFile('post_thumbnail')) {
                $post->post_thumbnail = $this->getThumbnail($request);
            }

            if (empty($post->post_thumbnail) && $request->has('youtube_id') ) {
                $post->post_thumbnail = $youtube->youtube_thumbnail;
            }
            // $request->has("save_draft") ? $post->active = 0 : $post->active = 1;
            $post->save();
            $request->session()->flash('status_action_post', 'Bài viết được cập nhật thành công');
            return redirect()->route('post.edit', $id);
        } else {
            return abort(401);
        }
    }

    public function destroy($id)
    {
        $post = Post::find($id);
        $user = $post->user;
        if ($post->user_id == Auth::user()->id || Auth::user()->user_level > $user->user_level) {        
            $post->destroy($id);
            return redirect()->route('listPost');
        } else {
            return abort(401);
        }        
    }

    public function getThumbnail($request){
        $target_dir = "uploads/images/";
        $file = $request->file('post_thumbnail');
        $file_name = time()."-".$file->getClientOriginalName();        
        $file->move($target_dir, $file_name);        
        $target_file = $target_dir.$file_name;
        $image = Image::make($target_file)->resize(320, 180)->save();
        
        return "/" . $target_file;
    }
}
//http://www.roxyfileman.com/
////http://test.albertoperipolli.com/filemanager4tinymce/