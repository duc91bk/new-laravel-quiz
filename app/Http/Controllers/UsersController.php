<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use Auth;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $users=User::orderBy('id')->paginate(20);
        return view('users.index', compact('users'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create($id)
    {
        if (Auth::user()->level < 3) {
            return view('users.create');
        } else {
            dd("Bạn không có quyền thực hiện thao tác này.");
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        if (Auth::user()->level < 3) {
            $user = new User;

            $user->name=$request->name;

            $user->description=$request->description;

            $user->level=$request->level;

            $user->email=$request->email;

            $user->password=bcrypt($request->password);

            $user->save();

            return redirect()->route('users.index');
        } else {
            dd("Bạn không có quyền thực hiện thao tác này.");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $user=User::find($id);
        return view('backends.pages.userProfile', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        if (Auth::user()->level < 3 || Auth::user()->id == $id) {
            $user = User::find($id);
           
            return view('users.edit', compact('users'));
        } else {
            dd("Bạn không có quyền thực hiện thao tác này.");
        }
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::user()->level < 3) {
            $user=User::find($id);      
        
            $user->name=$request->name;

            $user->description=$request->description;

            if (Auth::user()->id != $id) {
                $user->level=$request->level;
            }            

            $user->email=$request->email;

            // $user->password=bcrypt($request->password);

            $user->save();

            return redirect()->route('users.index');
        } else {
            dd("Bạn không có quyền thực hiện thao tác này.");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if (Auth::user()->level < 3 && Auth::user()->id != $id) {
            $user = User::find($id);
            $user->delete();
            return redirect()->route('users.index');
        } else {
            dd("Bạn không có quyền thực hiện thao tác này.");
        }
    }
}
