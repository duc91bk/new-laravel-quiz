<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Cat;
use Auth;

class CatsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return cats.index
     */
    public function index()
    {
        $cats=Cat
            ::orderBy('id' , 'desc')
            -> join('users', 'cats.user_id', '=', 'users.id')
            -> select('cats.*', 'users.name', 'users.user_level')
            -> paginate(20);

        return view('backends.pages.listCat', compact('cats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return cats.create
     */
    public function create()
    {
        return view('backends.pages.createCat');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CatFormRequest  $request
     * @return cats.index
     */
    public function store(Request $request)
    {
        $cat = new Cat;

        $cat->cat_title  = $request->cat_title;
        $cat->user_id   = Auth::user()->id;
        $cat->save();

        return redirect()->route('cats.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $cat = Cat::find($id);
        $posts = $cat->posts->toArray();
        $posts = array_reverse($posts);        

        return view('frontends.pages.listPostOfCat', compact('cat','posts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return cats.edit
     */
    public function edit($id)
    {
        $cat = Cat::find($id);
        $user = $cat->user;
        if ($cat->user_id == Auth::user()->id || (Auth::user()->user_level > 3 && Auth::user()->user_level > $user->user_level)) {
            return view('backends.pages.editCat', compact('cat'));
        } else {
            dd(" Ban không có quyền thực hiện thao tác này.");
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CatFormRequest  $request
     * @param  int  $id
     * @return cats.index
     */
    public function update(Request $request, $id)
    {
        $cat = Cat::find($id);
        $user = $cat->user;
        if ($cat->user_id == Auth::user()->id || (Auth::user()->user_level > 3 && Auth::user()->user_level > $user->user_level)) {         
            $cat->cat_title  = $request->cat_title;
            $cat->save();
            return redirect()->route('cats.index');
        } else {
            dd(" Ban không có quyền thực hiện thao tác này.");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return cats.index
     */
    public function destroy($id)
    {
        $cat = Cat::find($id);
        $user = $cat->user;
        if ($cat->user_id == Auth::user()->id || (Auth::user()->user_level > 3 && Auth::user()->user_level > $user->user_level)) {
            $cat->destroy($id);
            return redirect()->route('cats.index');
        } else {
            dd(" Ban không có quyền thực hiện thao tác này.");
        }
    }
}
