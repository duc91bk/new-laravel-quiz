<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Vocabulary;
use Auth;
use Image;

class VocabulariesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vocabularies = Vocabulary
            ::orderBy('created_at','desc')
            ->join('users', 'vocabularies.user_id', '=', 'users.id')
            ->select('vocabularies.*','users.name')
            ->paginate(20);
        return view('backends.pages.listVocabulary', compact('vocabularies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backends.pages.createVocabulary');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'vocabulary_title' => 'required|unique:vocabularies|min:2|max:50',
            'vocabulary_content' => 'required',
            'vocabulary_media' => 'required',
            'vocabulary_ipa' => 'required',
            'vocabulary_thumbnail' => 'required'

        ]);
        $vocabulary = new Vocabulary;
        $vocabulary->user_id = Auth::user()->id;
        $vocabulary->vocabulary_title = trim($request->vocabulary_title);
        $vocabulary->vocabulary_ipa = trim($request->vocabulary_ipa);
        if ($request->hasFile('vocabulary_thumbnail')) {
            $vocabulary->vocabulary_thumbnail = $this->getThumbnail($request);
        }
        $vocabulary->vocabulary_content = $request->vocabulary_content;
        if ($request->hasFile('vocabulary_media')) {
            $vocabulary->vocabulary_media = $this->getAudio($request);
        }
        
        $vocabulary->group_vocabulary_id = trim(str_replace(" ", "", $request->group_vocabulary_id),',');        
        $vocabulary->save();

        return redirect()->route('vocabulary.edit', $vocabulary->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vocabulary = Vocabulary::find($id);
        return view('frontends.pages.detailVocabulary', compact('vocabulary'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id , Request $request)
    {
        $vocabulary = Vocabulary::find($id);
        return view('backends.pages.editVocabulary', compact('vocabulary'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'vocabulary_title' => 'required|min:2|max:50',
            'vocabulary_content' => 'required'
        ]);

        $vocabulary = Vocabulary::find($id);
        $vocabulary->vocabulary_title = trim($request->vocabulary_title);
        $vocabulary->vocabulary_ipa = trim($request->vocabulary_ipa);
        if ($request->hasFile('vocabulary_thumbnail')) {
            $vocabulary->vocabulary_thumbnail = $this->getThumbnail($request);
        }
        $vocabulary->vocabulary_content = $request->vocabulary_content;
        if ($request->hasFile('vocabulary_media')) {
            $vocabulary->vocabulary_media = $this->getAudio($request);
        }
        $vocabulary->group_vocabulary_id = trim(str_replace(" ", "", $request->group_vocabulary_id),',');
        $vocabulary->save();

        return redirect()->route('vocabulary.edit', $vocabulary->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Get audio
     */
    public function getAudio($request)
    {
        $target_dir = "frontends/audio/";
        $file = $request->file('vocabulary_media');
        $file_name = time()."-".$file->getClientOriginalName();        
        $file->move($target_dir, $file_name);        
        $target_file = "/" . $target_dir.$file_name;        
        return $target_file;
    }    

    public function search(Request $request)
    {
        $searchKey = $request->search;
        $search = Vocabulary::where('vocabularies.vocabulary_title', '=', $searchKey)->select('vocabularies.id')->get()->toArray();  
        if (count($search) > 0) {
            $vocabularyId = $search[0]["id"];
            return redirect()->route('vocabulary.show', $vocabularyId);
        } else {
            return abort(404);
        }
              
    }

    public function getThumbnail($request){
        $target_dir = "uploads/vocabularies/";
        $file = $request->file('vocabulary_thumbnail');
        $file_name = time()."-".$file->getClientOriginalName();        
        $file->move($target_dir, $file_name);        
        $target_file =  $target_dir.$file_name;
        $image = Image::make($target_file)->resize(320, 180)->save();
        
        return "/" .$target_file;
    }
}
