<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Youtube;
use Auth;
class YoutubesController extends Controller
{
    public function index(Request $request)
    {
    	$youtubes=Youtube
            ::orderBy('id' , 'desc')
            -> join('users', 'youtubes.user_id', '=', 'users.id')
            -> select('youtubes.*', 'users.name')
            -> paginate(20);
    	return view('backends.pages.listYoutube', compact('youtubes'));
    }
    public function create(Request $request)
    {
    	return view('backends.pages.createYoutube');
    }
    public function store(Request $request)
    {
    	$youtube = new Youtube;
    	$this->validate($request, [
            'youtube_title' => 'required|unique:youtubes|min:3|max:255'
        ]);
    	$youtube->user_id =  Auth::user()->id;
        $youtube->youtube_title = $request->youtube_title;
        $youtubeId = $this->get_youtube_id($request);
        $youtube->youtube_code_id = $youtubeId;
        $youtube->youtube_thumbnail = "https://i.ytimg.com/vi/". $youtubeId ."/mqdefault.jpg";        
        $youtube->youtube_en = $request->youtube_en;
        $youtube->youtube_vn = $request->youtube_vn;
        $youtube->save();

        return redirect()->route('youtube.edit', $youtube->id);
    }
    public function edit(Request $request, $id)
    {
    	$youtube = Youtube::find($id);
    	return view('backends.pages.editYoutube', compact('youtube'));
    }
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'youtube_title' => 'required|min:3|max:255'
        ]);

    	$youtube = Youtube::find($id);
    	$youtube->youtube_title = $request->youtube_title;
    	$youtubeId = $this->get_youtube_id($request);
        $youtube->youtube_code_id = $youtubeId;
        $youtube->youtube_thumbnail = "https://i.ytimg.com/vi/". $youtubeId ."/mqdefault.jpg";        
        $youtube->youtube_en = $request->youtube_en;
        $youtube->youtube_vn = $request->youtube_vn;
        $youtube->save();
        return redirect()->route('youtubes.index');
    }
    public function destroy(Request $request, $id)
    {
    	$youtube = Youtube::find($id);
    	$youtube->destroy($id);
    	return redirect()->route('youtubes.index');
    }
    public function get_youtube_id($request){
        $url = $request->youtube_url;                 
        $queryString = parse_url($url, PHP_URL_QUERY);
        parse_str($queryString, $params);

        return $params['v'];
    }
}
