<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\GroupQuiz;
use App\Quiz;
use App\Answer;
use Auth;

class GroupQuizzesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return 
     */
    public function index()
    {        
        $group_quizzes = GroupQuiz
            ::orderBy('id', 'desc')
            ->join('users', 'group_quizzes.user_id', '=', 'users.id')
            ->select('group_quizzes.*','users.name')
            ->paginate(20);


        $ar_group_quizzes = GroupQuiz
            ::orderBy('id', 'desc')
            ->join('users', 'group_quizzes.user_id', '=', 'users.id')
            ->select('group_quizzes.*')
            ->get()->toArray();

        $ar_group_id = array();
        foreach ($ar_group_quizzes as $key => $value) {
            foreach ($value as $key1 => $value1) {
                if ($key1 == 'id') {
                    array_push($ar_group_id, $value1);    
                }
                
            }
        }
       

        $ar_number_quiz = array();
        foreach ($ar_group_id as $key => $value) {
            $countQuiz = Quiz
            ::join('group_quizzes', 'quizzes.group_quiz_id', '=', 'group_quizzes.id')
            ->where('quizzes.group_quiz_id', '=', $value)
            ->select('quizzes.*')            
            ->get()->count();    
            $ar_number_quiz[$value] = $countQuiz;
        }

        return view('backends.pages.listGroupquiz', compact('group_quizzes', 'ar_number_quiz'));
    }

    public function select_list_group_quiz(){

        $group_quizzes = GroupQuiz
            ::orderBy('created_at')
            ->join('users', 'group_quizzes.user_id', '=', 'users.id')
            ->where('group_quizzes.user_id','=',Auth::user()->id)
            ->select('group_quizzes.*','users.name')
            ->paginate(20);

        return view('group_quizzes.create_group_front_end', compact('group_quizzes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {     
        return view('backends.pages.createGroupQuiz');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $group_quiz = new GroupQuiz;

        $group_quiz->group_quiz_title = $request->group_quiz_title;


        $group_quiz->user_id = Auth::user()->id;

        if ($request->has("group_quiz_duration")) {
            $group_quizzes->group_quiz_duration=$request->group_quiz_duration;
        }        

        $group_quiz->save();

        return redirect()->route('group_quiz.edit', $group_quiz->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {

        $group_quiz=GroupQuiz::find($id);

        return view('backends.pages.editGroupQuiz', compact('group_quiz'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $group_quiz = GroupQuiz::find($id);
      
        $group_quiz->group_quiz_title = $request->group_quiz_title;

        $group_quiz->save();

        return redirect()->route('group_quizzes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $user_id = Auth::user()->id;
        $group_quiz=GroupQuiz::find($id);

        if ($user_id == $group_quiz->user_id) {
            $group_quiz->destroy($id);
            return redirect()->route('group_quizzes.index');
        } else {
            dd('Ban khong co quyen thuc hien thao tac nay');
        }        
    }

    function ajaxGetQuizzes(Request $request) {
        $groupQuizId = $request->groupQuizId;
        $groupQuiz = GroupQuiz::find($request->groupQuizId);
        // var_dump($groupQuiz);
        // dd('die');

        if ($groupQuiz != null) {        
            $quizzes = $groupQuiz->quizzes->toArray();

            $jsonDataQuiz = '{"group_quiz":{';
            $jsonDataQuiz .= '"title": "' . $groupQuiz->group_quiz_title . '",';
            $jsonDataQuiz .= '"id":"'. $groupQuiz->id .'",';
            $jsonDataQuiz .= '"quizs":[';
            $countQuiz = count($quizzes);
            for ($i=0; $i < $countQuiz; $i++) { 
                if ($i == 0) {
                    $jsonDataQuiz .= '{';
                } else {
                    $jsonDataQuiz .= ',{';
                }   
                    $jsonDataQuiz .= '"id":"'. $quizzes[$i]["id"] .'",';                 
                    $jsonDataQuiz .= '"title":"'. $quizzes[$i]["quiz_title"] .'",';
                    $jsonDataQuiz .= '"type":"'. $quizzes[$i]["quiz_type"] .'",';
                    $jsonDataQuiz .= '"description":"'. $quizzes[$i]["quiz_description"] .'",';
                    $jsonDataQuiz .= '"media":"'. $quizzes[$i]["quiz_media"] .'",';
                    $jsonDataQuiz .= '"answers":[';
                        $answers= Quiz::find($quizzes[$i]["id"])->answers->toArray();
                        $countAnswer = count($answers);
                        for ($j=0; $j < $countAnswer ; $j++) { 
                            if ($j == 0) {
                                $jsonDataQuiz .= '{';
                            } else {
                                $jsonDataQuiz .= ',{';
                            }
                            $jsonDataQuiz .= '"id":"'. $answers[$j]["id"] .'",';
                            $jsonDataQuiz .= '"title":"'. $answers[$j]["answer_title"] .'",';
                            $jsonDataQuiz .= '"thumbnail":"'. $answers[$j]["answer_thumbnail"] .'",';
                            $jsonDataQuiz .= '"is_correct":"'. $answers[$j]["answer_is_correct"] .'"';
                            $jsonDataQuiz .= '}';
                        }
                    $jsonDataQuiz .= ']';
                $jsonDataQuiz .= '}';
            }

            $jsonDataQuiz .= ']';
            $jsonDataQuiz .= '}}';

            return response($jsonDataQuiz);            
        }

    }

    function ajaxGetAllQuizzes() {
        $userHistory = explode(",", Auth::user()->user_history);
        $quizzes = array();

        for ($i=0; $i < count($userHistory) ; $i++) { 
            if (starts_with($userHistory[$i], 'group_quiz_')) {
                $groupQuiz = GroupQuiz::find(trim($userHistory[$i], 'group_quiz_'));
                $quiz = $groupQuiz->quizzes->toArray();                
                $quizzes = array_merge($quiz, $quizzes);
            }
        }

                

        // khi nguoi dung chua hoan thanh bai trac nghiem nao 
        if (count($quizzes) == 0) {
            $groupQuizId = GroupQuiz::select('group_quizzes.id')->get()->toArray();
            for ($i=0; $i < count($groupQuizId) ; $i++) { 
                $groupQuiz = GroupQuiz::find($groupQuizId[$i]["id"]);
                $quiz = $groupQuiz->quizzes->toArray();
                $quizzes = array_merge($quiz, $quizzes);
            }
        }

        $jsonDataQuiz = '{"group_quiz":{';
        $jsonDataQuiz .= '"title": "Luyện tập tổng quát",';
        $jsonDataQuiz .= '"id":"all",';
        $jsonDataQuiz .= '"quizs":[';
        $countQuiz = count($quizzes);
        for ($i=0; $i < $countQuiz; $i++) { 
            if ($i == 0) {
                $jsonDataQuiz .= '{';
            } else {
                $jsonDataQuiz .= ',{';
            }   
                $jsonDataQuiz .= '"id":"'. $quizzes[$i]["id"] .'",';                 
                $jsonDataQuiz .= '"title":"'. $quizzes[$i]["quiz_title"] .'",';
                $jsonDataQuiz .= '"type":"'. $quizzes[$i]["quiz_type"] .'",';
                $jsonDataQuiz .= '"description":"'. $quizzes[$i]["quiz_description"] .'",';
                $jsonDataQuiz .= '"media":"'. $quizzes[$i]["quiz_media"] .'",';
                $jsonDataQuiz .= '"answers":[';
                    $answers= Quiz::find($quizzes[$i]["id"])->answers->toArray();
                    $countAnswer = count($answers);
                    for ($j=0; $j < $countAnswer ; $j++) { 
                        if ($j == 0) {
                            $jsonDataQuiz .= '{';
                        } else {
                            $jsonDataQuiz .= ',{';
                        }
                        $jsonDataQuiz .= '"id":"'. $answers[$j]["id"] .'",';
                        $jsonDataQuiz .= '"title":"'. $answers[$j]["answer_title"] .'",';
                        $jsonDataQuiz .= '"thumbnail":"'. $answers[$j]["answer_thumbnail"] .'",';
                        $jsonDataQuiz .= '"is_correct":"'. $answers[$j]["answer_is_correct"] .'"';
                        $jsonDataQuiz .= '}';
                    }
                $jsonDataQuiz .= ']';
            $jsonDataQuiz .= '}';
        }

        $jsonDataQuiz .= ']';
        $jsonDataQuiz .= '}}';

        return response($jsonDataQuiz);   
    }
}
