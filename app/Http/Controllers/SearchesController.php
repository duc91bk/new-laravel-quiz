<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Post;
use App\Vocabulary;

class SearchesController extends Controller
{
    public function search(Request $request)
    {
    	$datas = array();
    	$searchKey = $request->searchKey;
    	$posts = Post::where('posts.post_title', 'like', "%$searchKey%")->get();
    	foreach ($posts as $post) {
    		array_push($datas, array('title'=>$post->post_title, 'url'=>route('post.show',$post->id)));
    	}
    	$vocabularies = Vocabulary::where('vocabularies.vocabulary_title', 'like', "%$searchKey%")->get();

    	foreach ($vocabularies as $vocabulary) {
    		array_push($datas, array('title'=>$vocabulary->vocabulary_title, 'url'=>route('vocabulary.show',$vocabulary->id)));
    	}

    	return view('frontends.pages.search', compact('datas', 'searchKey'));
    }
}
