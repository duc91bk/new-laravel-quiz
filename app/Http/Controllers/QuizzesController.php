<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;


use App\Quiz;
use App\Answer;
use App\GroupQuiz;
use App\User;
use Auth;
use Image;
use Carbon\Carbon;


class QuizzesController extends Controller
{
    public function index($id)
    {
        $quizzes = Quiz::orderBy('created_at','desc')
            ->where('quizzes.group_quiz_id', '=', $id)
            ->select('quizzes.id','quizzes.quiz_title')
            ->paginate(15);

        $ar_quizzes = Quiz::orderBy('created_at','desc')
            ->where('quizzes.group_quiz_id', '=', $id)
            ->select('quizzes.id','quizzes.quiz_title')
            ->get()->toArray();

        $ar_quiz_id = array();
        for ($i=0; $i < count($ar_quizzes); $i++) { 
            array_push($ar_quiz_id, $ar_quizzes[$i]["id"]);
        }

        $answers = Answer::whereIn('quiz_id', $ar_quiz_id)
            ->select('answers.answer_title', 'answers.quiz_id','answers.answer_is_correct')
            ->get();

        $group_quiz_title = GroupQuiz::where('group_quizzes.id', '=', $id)
            ->select('group_quizzes.id', 'group_quizzes.group_quiz_title')
            ->get()[0]->group_quiz_title;
   
       
        return view('backends.pages.listQuiz', compact('quizzes', 'group_quiz_title', 'answers'));
    }

    public function create()
    {
        $group_quizzes = GroupQuiz
            ::orderBy('created_at','desc')
            ->where('user_id', '=', Auth::user()->id)
            ->select('group_quizzes.*')
            ->get();
        $arGroupQuizzes = array();
        foreach ($group_quizzes as $group_quiz) {
            $arGroupQuizzes[$group_quiz->id] = $group_quiz->group_quiz_title;
        }

        return view('backends.pages.createQuiz', compact('arGroupQuizzes'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'quiz_title' => 'required|min:3|max:255',
            'group_quiz_id' => 'required',
        ]);
        $group_quiz_id = $request->group_quiz_id;
        $quiz_type = $request->quiz_type;

        /**
         * table quizzes
         * @var quiz
         */

        $quiz = new Quiz;

        $quiz->quiz_title = $request->quiz_title;
        $quiz->quiz_description = $request->quiz_description;
        if ($request->hasFile("quiz_media")) {
            $quiz->quiz_media = $this->getThumbnail($request, "quiz_media");
        }        
        $quiz->group_quiz_id = $group_quiz_id;        
        $quiz->quiz_type = $quiz_type;

        $quiz->user_id = Auth::user()->id;

        $quiz->save();
        
        /**
         * table answers
         * @var 
         */
        
        $is_correct  = $request->answer_is_correct;        

        $LastInsertId = $quiz->id;
        $last_quiz = Quiz::find($LastInsertId);

        if ($quiz_type == "select") {
            for ($i=0; $i < 4; $i++) {
                $nameAnswer = "answers_".$i;
                $answer_title = $request->$nameAnswer;
                $thumbnail = 'thumbnail'.$i;
                if ($request->hasFile($thumbnail) && $request->file($thumbnail)->isValid()) {
                    $answerThumbnail = $this->getThumbnail($request, $thumbnail);
                } else {
                    $answerThumbnail = "";
                }
                $is_correct == $i ? $answer_is_correct = 1 : $answer_is_correct = 0;
                
                $last_quiz->answers()->saveMany([
                    new Answer([
                        'answer_title'   => $answer_title,
                        'answer_thumbnail'   => $answerThumbnail,
                        'answer_is_correct' => $answer_is_correct
                        ])
                ]);
            }
        } else if ($quiz_type == "input") {
            $num_answer = $request->num_answer;
            for ($i=0; $i < $num_answer ; $i++) {
                $nameAnswer = "answer_text_".$i;
                $answerTitle = $request->$nameAnswer;
                
                if (!empty($answerTitle)) {
                    $last_quiz->answers()->saveMany([
                        new Answer([
                            'answer_title'   => $answerTitle,
                            'answer_is_correct' => 1
                            ])                        
                    ]);
                }
            }
        }

        return redirect()->route('quiz.edit', $quiz->id);

    }

    public function result(Request $request)
    {
        $ar_result = $request->ar_result;

        $trueAnswer = 0;

        $ar_result =(array) json_decode($ar_result);

        foreach ($ar_result as $key => $value) {
            $quiz = Quiz::find($key);
            $answers = $quiz->answers;
            foreach ($answers as $answer) {
                if ($value == $answer->answer_title && $answer->answer_is_correct == 1) {
                  $trueAnswer += 1;
                  break;
                }
            }
        }

        $postId = $request->session()->get('postId');        
        $postTitle = $request->session()->get('postTitle');
        $groupQuizId = $request->session()->get('groupQuizId');
        $countQuiz = $request->session()->get('countQuiz');

        $resultScore = round($trueAnswer/$countQuiz, 1) * 10;

        $user = User::find(Auth::user()->id);

        if ($resultScore > 7) {
            $user->user_score += $resultScore;

            if ($user->user_score > 200) {
                
            }

            if ($user->user_score > 500) {
                
            }

            $user->save();
        }
        
        $quizzes = GroupQuiz::find($groupQuizId)->quizzes->toArray();

        $answers = array();
        for ($i=0; $i < count($quizzes); $i++) { 
            $answer= Quiz::find($quizzes[$i]["id"])->answers->toArray();                       
            $answers = array_merge($answers,$answer);
        } 

        return view('frontends.pages.resultQuiz', compact('postTitle', 'postId',  'countQuiz', 'ar_result', 'answers', 'groupQuizId', 'quizzes', 'resultScore', 'trueAnswer'));
    }

    public function edit($id)
    {
        $quiz = Quiz::find($id);
        $answers = $quiz->answers;

        $group_quizzes = GroupQuiz
            ::orderBy('created_at','desc')
            ->where('user_id', '=', Auth::user()->id)
            ->select('group_quizzes.*')
            ->get();

        return view('backends.pages.editQuiz', compact('quiz', 'group_quizzes', 'answers'));        
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'quiz_title' => 'required|min:3|max:255',
            'group_quiz_id' => 'required',
        ]);

        $group_quiz_id = $request->group_quiz_id;
        $quiz_type = $request->quiz_type;

        /**
         * table quizzes
         * @var quiz
         */

        $quiz = Quiz::find($id);
        $quiz->quiz_title = $request->quiz_title; 
        $quiz->quiz_description = $request->quiz_description;
        if ($request->hasFile("quiz_media")) {
            $quiz->quiz_media = $this->getThumbnail($request, "quiz_media");
        }            
        $quiz->group_quiz_id = $group_quiz_id;        
        $quiz->save();
        
        /**
         * table answers
         * @var 
         */

        $is_correct  = $request->answer_is_correct;
        $LastInsertId = $quiz->id;
        $last_quiz = Quiz::find($LastInsertId);


        if ($quiz_type == "select") {
            for ($i=0; $i < 4; $i++) {
                $nameAnswer = "answers_".$i;
                $answer_title = $request->$nameAnswer;
                $thumbnail = 'thumbnail'.$i;
                $request->hasFile($thumbnail) ? $answerThumbnail = $this->getThumbnail($request, $thumbnail) : $answerThumbnail = "";
                $is_correct == $i ? $answer_is_correct = 1 : $answer_is_correct = 0;                
                $name_id_answer = "id_answers_".$i;  
                
                $answer = Answer::find($request->$name_id_answer);

                $answer->answer_title = $answer_title;
                $answer->answer_is_correct = $answer_is_correct;
                $answer->answer_thumbnail = $answerThumbnail;
                
                $last_quiz->answers()->saveMany([
                    $answer,             
                ]);
            }
        } else if ($quiz_type == "input") {
            $num_answer= $request->num_answer;
            for ($i=0; $i < $num_answer ; $i++) { 
                if (!empty($request->answer_text[$i])) {
                    $id_answer = $request->id_answers[$i];
                    $answer = Answer::find($id_answer);
                    $answer->answer_title = trim($request->answer_text[$i]);
                    $answer->answer_is_correct = 1;
                    $last_quiz->answers()->saveMany([
                        $answer,                    
                    ]);
                }
            }            
        }        

        return redirect()->route('quizzes.index', $group_quiz_id);
    }

    public function destroy($id)
    {
        $quiz = Quiz::find($id);

        $group_quiz = $quiz->group_quizzes;
        $answers = $quiz->answers;

        if (Auth::user()->id == $group_quiz->user_id) {
            $quiz->destroy($id);
            foreach ($answers as $answer) {
                $answer->destroy($answer->id);
            }
            return redirect()->route('quizzes.index', $group_quiz->id);
        } else {
            dd('Ban khong co quyen thuc hien thao tac nay');
        }        
    }

    public function getThumbnail($request,$thumbnail){
        $target_dir = "frontends/images/uploads/quizzes/";
        $file = $request->file($thumbnail);
        $file_name = time()."-".$file->getClientOriginalName();        
        $file->move($target_dir, $file_name);        
        $target_file = $target_dir.$file_name;
        if (str_contains($thumbnail, 'thumbnail')) {
            $image = Image::make($target_file)->resize(200, 200)->save();
        }
        
        return "/" . $target_file;
    }

    public function plusScore(Request $request) 
    {
        // $userId = $request->userId;
        $jsonResult = $request->jsonResult;
        
        $groupQuizId = $request->groupQuizId;

        // if ($userId == Auth::user()->id) 
        // {
            for ($i=0; $i < count($jsonResult['result']) ; $i++) 
            { 
                $quiz = Quiz::find($jsonResult['result'][$i]['quiz_id']);
                $answer = Answer::find($jsonResult['result'][$i]['answer_id']);        

                if ($quiz->quiz_type == 'select') {
                    if ($jsonResult['result'][$i]['quiz_id'] == $answer->quiz_id &&
                        $answer->answer_is_correct == 1 ) {
                        continue;
                    } else {
                        return ["status"=>2, "notify"=>"Hệ thống nhận thấy có sai xót trong bài kiểm tra của bạn. Bạn vui lòng làm lại bài kiểm tra để được tích lũy điểm."];
                    }
                } else {
                    if ($jsonResult['result'][$i]['quiz_id'] == $answer->quiz_id &&
                        $jsonResult['result'][$i]['answer_from_user'] == $answer->answer_title
                        ) 
                    {
                        continue;
                    } else {
                        return ["status"=>2, "notify"=>"Hệ thống nhận thấy có sai xót trong bài kiểm tra của bạn. Bạn vui lòng làm lại bài kiểm tra để được tích lũy điểm."];
                    }
                }
                
            }

            $user = User::find(Auth::user()->id);
            $currentTime = $newIime = Carbon::now();
            $currentTime = $currentTime->month . $currentTime->day;

            // set session history group quiz
            $oldUserHistory = explode(',', $user->user_history);
            if ($groupQuizId != 0) {
                $idHistory = "group_quiz_" . $groupQuizId;
                if (!in_array($idHistory, $oldUserHistory)) {
                    array_push($oldUserHistory, $idHistory);
                } else {
                    for ($j=0; $j < count($oldUserHistory); $j++) { 
                        if ($oldUserHistory[$j] == $idHistory) {
                            unset($oldUserHistory[$j]);
                            array_push($oldUserHistory, $idHistory);
                            break;
                        }
                    }
                }
            } else {               
                $hasAllGroupQuiz = false;
                for ($i=0; $i < count($oldUserHistory); $i++) { 
                    if (starts_with($oldUserHistory[$i], 'all_group_quiz_')) {
                        $numberComplete = trim($oldUserHistory[$i], 'all_group_quiz_') + 1;
                        $oldUserHistory[$i] = 'all_group_quiz_' . $numberComplete;
                        $hasAllGroupQuiz = true;
                        break;
                    }
                }
                if ($hasAllGroupQuiz == false) {
                    array_push($oldUserHistory, 'all_group_quiz_1');
                }
            }
            
            $oldUserHistory = implode(",", $oldUserHistory);
            $user->user_history = $oldUserHistory;
            $user->save();


            // set session so lan duoc cong diem trong ngay
            $verifyPlus = false;
            if ($groupQuizId != 0) {
                $sessionQuiz = "sessionQuiz:" . Auth::user()->id . "," . $groupQuizId;
                
                if (!$request->session()->has($sessionQuiz)) 
                {
                    $request->session()->put($sessionQuiz, "$currentTime" . "," . 1);
                    $verifyPlus = true;
                }
                else 
                {
                    $oldSession = explode(",", $request->session()->get($sessionQuiz));

                    $oldIime = $oldSession[0];
                    $oldNumber = $oldSession[1];

                    if ($currentTime == $oldIime && $oldNumber < 3) {
                        $oldNumber += 1;
                        $request->session()->put($sessionQuiz, "$currentTime" . "," . $oldNumber);
                        $verifyPlus = true;
                    } 
                    elseif($currentTime == $oldIime && $oldNumber >= 3) 
                    {
                        return ["status"=>1, "notify"=>"Chúc mừng bạn đã hoàn thành gói trắc nghiệm. Bạn đã vượt quá số lần được cộng điểm cho phép với bài trắc nghiệm này trong ngày."];
                    } 
                    elseif($currentTime != $oldIime)
                    {
                        $request->session()->put($sessionQuiz, "$currentTime" . "," . 1);
                        $verifyPlus = true;
                    }

                }
            } else {
                $verifyPlus = true;
            }

            if ($verifyPlus == true) {
                $user->user_score += 10;
                $userHistory = explode(",", $user->user_history);
                $numberGroupQuiz = 0;
                $numberAllGroupQuiz = 0;
                for ($i=0; $i < count($userHistory); $i++) { 
                    if (starts_with($userHistory[$i], 'group_quiz_')) {
                        $numberGroupQuiz += 1;
                    }

                    if (starts_with($userHistory[$i], 'all_group_quiz_')) {
                        $numberAllGroupQuiz += 1;
                    }
                }


                if ($user->user_score >= 2000 && $numberGroupQuiz >= 160 && $numberAllGroupQuiz >= 40) {
                    $user->user_level = 5; 
                }
                elseif ($user->user_score >= 1000 && $numberGroupQuiz >= 80 && $numberAllGroupQuiz >= 20) {
                    $user->user_level = 4;   
                }
                elseif ($user->user_score >= 500 && $numberGroupQuiz >= 40 && $numberAllGroupQuiz >= 10) {
                    $user->user_level = 3;  
                }
                elseif ($user->user_score >= 250 && $numberGroupQuiz >= 20 && $numberAllGroupQuiz >= 5) {
                    $user->user_level = 2;
                }


                $user->save();

                return ["status"=>1, "notify"=>"Chúc mừng bạn đã hoàn thành bài trắc nghiệm. Bạn được cộng thêm 10 điểm."];
            }
        // } 
        // else 
        // {
        //     return ["status"=>2, "notify"=>"Hệ thống nhận thấy có sai xót trong bài kiểm tra của bạn. Bạn vui lòng làm lại bài kiểm tra để được tích lũy điểm."];
        // }        
    }
}
