<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Post;
use App\Cat;
use App\Vocabulary;
use Auth;

class AjaxController extends Controller
{
    public function searchSuggest()
    {
    	$data = array();
    	$vocabularies = Vocabulary::select('vocabularies.vocabulary_title')->get()->toArray();
    	for ($i=0; $i < count($vocabularies); $i++) { 
    		array_push($data, $vocabularies[$i]['vocabulary_title']);
    	}

    	$posts = Post::select('posts.post_title')->get()->toArray();
    	for ($i=0; $i < count($posts) ; $i++) { 
    		array_push($data, $posts[$i]['post_title']);
    	}

    	return response()->json($data);
    }
}
