<?php

namespace App\Http\Middleware;

use Closure;
use Menu;
use Auth;

class DashboardSidebarMenu
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Menu::make('DashboardSidebarMenu', function($menu){

            if (Auth::user()->user_level > 3) {
                // post
                $posts = $menu->add('Posts', array( 'class' => 'treeview'));

                $menu->posts ->append('</span> <i class="fa fa-angle-left pull-right"></i>')
                            ->prepend(' <i class="fa fa-edit"></i> <span>');

                $menu->posts ->add('All Posts', array('route'  => 'listPost'));
                $menu->posts ->add('Add New', array('route'  => 'post.create'));
                $menu->posts ->add('All Categories', array('route'  => 'cats.index'));
                $menu->posts ->add('Add Category', array('route'  => 'cat.create'));

                // quiz
                $quizs = $menu->add('Quizs', array( 'class' => 'treeview'));

                $menu->quizs ->append('</span> <i class="fa fa-angle-left pull-right"></i>')
                            ->prepend(' <i class="fa fa-edit"></i> <span>');

                $menu->quizs ->add('Add Quiz', array('route'  => 'quiz.create'));
                $menu->quizs ->add('All Group Quiz', array('route'  => 'group_quizzes.index'));
                $menu->quizs ->add('Add Group Quiz', array('route'  => 'group_quiz.create'));

                // Youtubes
                $youtubes = $menu->add('Youtubes', array('class' => 'treeview'));
                $menu->youtubes ->append('</span> <i class="fa fa-angle-left pull-right"></i>')
                                ->prepend(' <i class="fa fa-edit"></i> <span>');
                $menu->youtubes ->add('Add Youtube', array('route'  => 'youtube.create'));
                $menu->youtubes ->add('All Youtube', array('route'  => 'youtubes.index'));

                // Vocabularies
                $vocabularies = $menu->add('Vocabularies', array('class' => 'treeview'));
                $menu->vocabularies ->append('</span> <i class="fa fa-angle-left pull-right"></i>')
                                ->prepend(' <i class="fa fa-edit"></i> <span>');            
                $menu->vocabularies ->add('All Vocabulary', array('route'  => 'vocabularies.index'));
                $menu->vocabularies ->add('Add Vocabulary', array('route'  => 'vocabulary.create'));    

            }

            // users
            $users = $menu->add('Users', array( 'class' => 'treeview'));

            $menu->users ->append('</span> <i class="fa fa-angle-left pull-right"></i>')
                        ->prepend(' <i class="fa fa-edit"></i> <span>');

            if (Auth::user()->user_level > 5) {
                $menu->users->add('All Users', array('route'  => 'users.index'));
                $menu->users->add('Add user',    array('route'  => array('user.create', 'id' => Auth::user()->id)));
            }

            $menu->users->add('Your Profile',    array('route'  => array('user.profile', 'id' => Auth::user()->id)));

        });
        return $next($request);
    }
}
