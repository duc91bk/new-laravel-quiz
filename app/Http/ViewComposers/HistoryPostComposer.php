<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use Illuminate\Http\Request;
use Auth;
use App\Post;
use App\User;

class HistoryPostComposer
{

    protected $postsHistoryPost;
    
    public function __construct(Request $request)
    {
        if (Auth::check()) {
            $user = User::find(Auth::user()->id);

            if ($user->user_history != "") {
                $oldUserHistory = explode(',', $user->user_history);
                $arHistoryPost = array();

                for ($j=0; $j < count($oldUserHistory); $j++) {
                    if (starts_with($oldUserHistory[$j], 'post_')) {
                        array_push($arHistoryPost, trim($oldUserHistory[$j], 'post_'));
                    } 
                }
                $this->postsHistoryPost = Post::whereIn('id', $arHistoryPost)->take(6)->get()->toArray();
            }
        }
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('postsHistoryPost', $this->postsHistoryPost);
    }
}