<?php

Route::group(['middleware' => ['web']], function () {
	Route::get('ajaxResultQuiz', [
		'as' => 'ajaxResultQuiz',
		'uses' => 'QuizzesController@plusScore'
	]);

	Route::get('ajaxGetQuizzes', [
		'as' => 'ajaxGetQuizzes',
		'uses' => 'GroupQuizzesController@ajaxGetQuizzes'
	]);

	Route::get('ajaxGetAllQuizzes', [
		'as' => 'ajaxGetAllQuizzes',
		'uses' => 'GroupQuizzesController@ajaxGetAllQuizzes'
	]);

	Route::get('searchSuggest', [
		'as'=>'searchSuggest',
		'uses'=>'AjaxController@searchSuggest'
	]);


	Route::group(['middleware' => 'FrontendTopMenu'], function () {
    	Route::get('/', [
    		'as' => 'home',
    		'uses' => 'PostsController@index'
    	]);
    	/**
		 * Post
		 */
		Route::get('load_more_post', [
			'as'=> 'load.more.post',
			'uses'=> 'PostsController@ajaxIndex'
		]);
		Route::get('post/{id}',[
			'as'  => 'post.show',
			'uses' => 'PostsController@show'
		])->where('id', '[0-9]+');

		Route::get('post/user/{id}',[
			'as'  => 'post.listOfUser',
			'uses' => 'PostsController@listOfUser'
		])->where('id', '[0-9]+');
		
		Route::get('post/{postType}',[
			'as'  => 'post.listOfType',
			'uses' => 'PostsController@listOfType'
		])->where('name', '[A-Za-z]+');

		/**
		 * Quiz
		 */
		Route::post('result',[		
			'as'=>'quiz.result',
			'uses'=>'QuizzesController@result'
		]);

		/**
		 * Vocabulary
		 */
		
		Route::get('vocabulary/{id}',[
			'as'  => 'vocabulary.show',
			'uses' => 'VocabulariesController@show'
		])->where('id', '[0-9]+');


		Route::get('search',[
			'as'=>'search',
			'uses' => 'SearchesController@search'
		]);

		/**
		 * Cat
		 */

		Route::get('cat/{id}',[			
			'as'=>'cat.show',
			'uses'=>'CatsController@show'
		])->where('id', '[0-9]+');
		
	});

	Route::group(['middleware' => ['auth', 'DashboardSidebarMenu', 'CheckPermission'], 'prefix' => 'dashboard'], function () {
		/**
		 * Posts
		 */
		Route::get('posts',[
			'as'=>'listPost',
			'uses' => 'PostsController@listPost'
		]);

		Route::get('post/create',[			
			'as'=>'post.create',
			'uses'=>'PostsController@create'
		]);

		Route::post('post',[			
			'as'=>'post.store',
			'uses'=>'PostsController@store'
		]);


		Route::get('post/{id}/edit',[			
			'as'  => 'post.edit',
			'uses' => 'PostsController@edit'
		])->where('id', '[0-9]+');

		Route::put('post/{id}',[			
			'as'=>'post.update',
			'uses'=>'PostsController@update'
		])->where('id', '[0-9]+');

		Route::get('post/{id}/destroy',[			
			'as'  => 'post.destroy',
			'uses' => 'PostsController@destroy'
		]);

		Route::delete('posts/destroy_list_items',[		
			'as'  => 'posts.destroy_list_items',
			'uses' => 'PostsController@destroy_list_items'
		]);		

		/**
		 * Cat
		 */
		Route::get('cats',[		
			'as'=>'cats.index',
			'uses'=>'CatsController@index'
		]);
		Route::get('cat/create',[				
			'as'=>'cat.create',
			'uses'=>'CatsController@create'
		]);

		Route::post('cat',[			
			'as'=>'cat.store',
			'uses'=>'CatsController@store'
		]);

		Route::get('cat/{id}/edit',[			
			'as'=>'cat.edit',
			'uses'=>'CatsController@edit'
		])->where('id', '[0-9]+');

		Route::put('cat/{id}',[			
			'as'=>'cat.update',
			'uses'=>'CatsController@update'
		])->where('id', '[0-9]+');

		Route::get('cat/{id}/destroy',[			
			'as'=>'cat.destroy',
			'uses'=>'CatsController@destroy'
		])->where('id', '[0-9]+');

		/**
		 * Quizzes
		 */
		
		Route::get('quiz/create',[		
			'as'=>'quiz.create',
			'uses'=>'QuizzesController@create'
		]);

		Route::post('quiz/store',[		
			'as'=>'quiz.store',
			'uses'=>'QuizzesController@store'
		]);

		Route::get('quiz/{id}/edit',[		
			'as'=>'quiz.edit',
			'uses'=>'QuizzesController@edit'
		])->where('id', '[0-9]+');

		Route::put('quiz/{id}',[		
			'as'=>'quiz.update',
			'uses'=>'QuizzesController@update'
		])->where('id', '[0-9]+');

		Route::get('quiz/{id}/destroy',[		
			'as'=>'quiz.destroy',
			'uses'=>'QuizzesController@destroy'
		])->where('id', '[0-9]+');

		/**
		 * Group quiz
		 */
		Route::get('group_quizzes',[		
			'as'=>'group_quizzes.index',
			'uses'=>'GroupQuizzesController@index'
		]);
		Route::get('group_quiz/{id}',[
			'as'=>'quizzes.index',
			'uses'=>'QuizzesController@index'
		])->where('id', '[0-9]+');
		Route::get('group_quiz/create',[		
			'as'=>'group_quiz.create',
			'uses'=>'GroupQuizzesController@create'
		]);

		Route::post('group_quiz',[		
			'as'=>'group_quiz.store',
			'uses'=>'GroupQuizzesController@store'
		]);

		Route::get('group_quiz/{id}/edit',[		
			'as'=>'group_quiz.edit',
			'uses'=>'GroupQuizzesController@edit'
		])->where('id', '[0-9]+');

		Route::put('group_quiz/{id}',[			
			'as'=>'group_quiz.update',
			'uses'=>'GroupQuizzesController@update'
		])->where('id', '[0-9]+');

		Route::get('group_quiz/{id}/destroy',[			
			'as'=>'group_quiz.destroy',
			'uses'=>'GroupQuizzesController@destroy'
		])->where('id', '[0-9]+');

		/**
		 * 
		 * Youtube
		 * 
		 */
		
		Route::get('youtubes',[
			'as'=>'youtubes.index',
			'uses' => 'YoutubesController@index'
		]);

		Route::get('youtube/create',[			
			'as'=>'youtube.create',
			'uses'=>'YoutubesController@create'
		]);
		Route::post('youtube',[			
			'as'=>'youtube.store',
			'uses'=>'YoutubesController@store'
		]);
		Route::get('youtube/{id}/edit',[			
			'as'=>'youtube.edit',
			'uses'=>'YoutubesController@edit'
		])->where('id', '[0-9]+');
		Route::put('youtube/{id}',[			
			'as'=>'youtube.update',
			'uses'=>'YoutubesController@update'
		])->where('id', '[0-9]+');
		Route::get('youtube/{id}/destroy',[			
			'as'=>'youtube.destroy',
			'uses'=>'YoutubesController@destroy'
		])->where('id', '[0-9]+');

		/**
		 * 
		 * Vocabulary
		 * 
		 */
		
		Route::get('vocabularys',[
			'as'=>'vocabularies.index',
			'uses' => 'VocabulariesController@index'
		]);

		Route::get('vocabulary/create',[			
			'as'=>'vocabulary.create',
			'uses'=>'VocabulariesController@create'
		]);
		Route::post('vocabulary',[			
			'as'=>'vocabulary.store',
			'uses'=>'VocabulariesController@store'
		]);
		Route::get('vocabulary/{id}/edit',[			
			'as'=>'vocabulary.edit',
			'uses'=>'VocabulariesController@edit'
		])->where('id', '[0-9]+');
		Route::put('vocabulary/{id}',[			
			'as'=>'vocabulary.update',
			'uses'=>'VocabulariesController@update'
		])->where('id', '[0-9]+');
		Route::get('vocabulary/{id}/destroy',[			
			'as'=>'vocabulary.destroy',
			'uses'=>'VocabulariesController@destroy'
		])->where('id', '[0-9]+');

		
		
		


		/**
		 * 
		 * Search
		 * 
		 */
		

		
		// Route::get('search',[
		// 	'as'=>'dashboard.search.post',
		// 	'uses' => 'SearchController@getSearchPostdashboard'
		// ]);


	});

	Route::group(['middleware' => ['auth', 'DashboardSidebarMenu'], 'prefix' => 'dashboard'], function () {
		/**
		 * 
		 * User
		 * 
		 */

		Route::get('users',[
			
			'as'=>'users.index',
			'uses'=>'UsersController@index'
		]);

		Route::get('user/{id}/create',[
			
			'as'=>'user.create',
			'uses'=>'UsersController@create'
		]);

		Route::post('user',[
				
			'as'=>'user.store',
			'uses'=>'UsersController@store'
		]);

		Route::get('users/{id}/edit',[
			
			'as'  => 'user.edit',
			'uses' => 'UsersController@edit'
		]);

		Route::put('users/{id}',[
			
			'as'=>'user.update',
			'uses'=>'UsersController@update'
		]);

		Route::get('users/{id}/destroy',[
			
			'as'  => 'user.destroy',
			'uses' => 'UsersController@destroy'
		]);

		Route::get('user/{id}',[			
			'as'=>'user.profile',
			'uses'=>'UsersController@show'
		]);
	});


	

	// Route::get('/search',[
	// 	'as'=>'search.post',
	// 	'uses' => 'SearchController@getSearchPost'
	// ]);

	
	/*
	*
	*Authen
	* 
	 */

	Route::auth();
    Route::get('dashboard', 'HomeController@index');
});

