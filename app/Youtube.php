<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Youtube extends Model
{
    protected $fillable = [
        'user_id', 'youtube_en', 'youtube_vn', 'youtube_title', 'youtube_code_id'
    ];

    public function post()
    {
        return $this->hasMany('App\User','youtube_id','id');
    }
}
