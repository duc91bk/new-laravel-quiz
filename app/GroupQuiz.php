<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupQuiz extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'group_quiz_title'
    ];

    /**
     * Get the quizzes for the group quiz.
     */
    public function quizzes()
    {
        return $this->hasMany('App\Quiz','group_quiz_id','id');
    }

    

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];
}
