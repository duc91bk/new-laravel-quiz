<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Quiz;

class QuizzesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();        

        for ($i=0; $i < 1; $i++) {
            $quiz = new Quiz;
            $quiz->user_id = 1;
            $quiz->quiz_title = $faker->text($maxNbChars = 50);
            $quiz->quiz_type = 'select';
            $quiz->group_quiz_id = 1;
            $quiz->save();         
        }
    }
}
