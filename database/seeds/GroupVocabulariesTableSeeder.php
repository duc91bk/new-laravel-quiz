<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\GroupVocabulary;

class GroupVocabulariesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();        

        for ($i=0; $i < 5; $i++) {
            $groupVocabulary = new GroupVocabulary;
            $groupVocabulary->user_id = 1;
            $groupVocabulary->group_vocabulary_title = $faker->text($maxNbChars = 50);
            $groupVocabulary->group_vocabulary_thumbnail = $faker->imageUrl($width = 320, $height = 180);
            $groupVocabulary->group_quiz_id = 1;
            $groupVocabulary->save();         
        }
    }
}
