<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Post;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();      

        for ($i=0; $i < 24; $i++) {
            $post = new Post;
            $post->user_id = 2;
            $post->cat_id = 1;
            $post->group_quiz_id = 1;
            $post->post_title = $faker->text($maxNbChars = 100);
            $post->post_thumbnail = $faker->imageUrl($width = 400, $height = 300);
            $post->post_content = "";
            $post->post_type = 'standard';
            $post->youtube_id = 1;
            $post->post_view_count = 0;
            $post->save();         
        }
    }
}
