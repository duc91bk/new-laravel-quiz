<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\GroupQuiz;

class GroupQuizzesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();        

        for ($i=0; $i < 1; $i++) {
            $group_quiz = new GroupQuiz;
            $group_quiz->user_id = 1;
            $group_quiz->group_quiz_title = $faker->text($maxNbChars = 50);
            $group_quiz->save();         
        }
    }
}
