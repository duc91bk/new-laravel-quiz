<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();        

        for ($i=0; $i < 20; $i++) {
            $user = new User;
            $user->user_level=$faker->numberBetween($min = 1, $max = 5);
            $user->name=$faker->text($maxNbChars = 50);            
            $user->user_score=$faker->numberBetween($min = 1, $max = 1000);
            $user->email=$faker->email;
            $user->password=bcrypt('secret');
            $user->save();         
        }
    }
}
