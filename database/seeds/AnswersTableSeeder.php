<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Answer;

class AnswersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();        

        for ($i=0; $i < 1; $i++) {
            $answer = new Answer;
            $answer->quiz_id = 2;
            $answer->answer_title = $faker->text($maxNbChars = 50);
            $answer->answer_thumbnail = $faker->imageUrl($width = 320, $height = 180);
            $answer->answer_is_correct = 1;
            $answer->save();         
        }
    }
}