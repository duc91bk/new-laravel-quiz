<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Vocabulary;

class VocabulariesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();        

        for ($i=0; $i < 6; $i++) {
            $vocabulary = new Vocabulary;
            $vocabulary->user_id = 1;
            $vocabulary->vocabulary_title = $faker->text($maxNbChars = 30);
            $vocabulary->vocabulary_ipa = "/sdfsdf/";
            $vocabulary->vocabulary_thumbnail = $faker->imageUrl($width = 320, $height = 180);
            $vocabulary->vocabulary_content = $faker->paragraph($nbSentences = 5, $variableNbSentences = true);
            $vocabulary->vocabulary_media = '/frontends/audio/1458025683-abide_by.mp3';
            $vocabulary->post_id = 72;
            $vocabulary->save();         
        }
    }
}
