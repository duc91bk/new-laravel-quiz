$(document).ready(function() {
	input_checkbox ();
	upload_thumbnail();
	editor ();
	select_quiz_type();
	check_time_to_quiz();
	add_more_different_answer();
	admin_sidebar_menu();
	show_thumbnail();
	
});


/**
 * Editor
 * @return 
 */
function editor () {
	if ($('.post_form #post_content').length > 0) {	
		tinymce.init({
		    selector: ".post_form #post_content",
		    theme: "modern",
		    min_height: 500,
		    plugins: [
		         "advlist autolink link image lists charmap print preview hr anchor pagebreak",
		         "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
		         "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
		   ],
		   toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
		   toolbar2: "| responsivefilemanager | link unlink anchor | image media | forecolor backcolor  | print preview code ",
		   image_advtab: true ,
		   
		   external_filemanager_path:"/filemanager/",
		   filemanager_title:"Filemanager" ,
		   external_plugins: { "filemanager" : "/filemanager/plugin.min.js"}
		 });
	}
}

/**
 * @des: Get src image
 * @return 
 */
function upload_thumbnail () {
	var myReader = new FileReader();
	$(".thumbUpload").on('change', function () { 
		var $this = $(this);
        if (typeof (FileReader) != "undefined") { 
            var image_holder = $this.closest('.wrap-thumb').find(".image-holder");
            image_holder.empty();
 			image_holder.addClass('show');
            var reader = new FileReader();
            reader.onload = function (e) {
                $("<img />", {
                    "src": e.target.result,
                    "class": "thumb-image"
                }).appendTo(image_holder); 
            }
            image_holder.show();
            reader.readAsDataURL($(this)[0].files[0]);
        } else {
            alert("This browser does not support FileReader.");
        }
    });
}

/**
 * @des: Input checkbox
 * @return 
 */
function input_checkbox () {
	if ($('.auth_form input').length > 0) {
		$('.auth_form input').iCheck({
	      checkboxClass: 'icheckbox_square-blue',
	      radioClass: 'iradio_square-blue',
	      increaseArea: '20%' // optional
	    });
	}
}


function select_quiz_type(){
	$('#quiz_type').change(function(){
		if ($('#quiz_type').val() == 'select') {
			console.log($('#quiz_type').val());
			$('.quiz_select').removeClass('hidden').addClass('show');			
			$('.answers').removeClass('hidden').addClass('show');
			$('.quiz_input, .outer-different-answers').removeClass('show').addClass('hidden');
			$()
		} else {
			console.log($('#quiz_type').val());
			$('.quiz_select').removeClass('show').addClass('hidden');
			$('.answers').removeClass('show').addClass('hidden');
			$('.quiz_input, .outer-different-answers').removeClass('hidden').addClass('show');
		}
	});
}

function check_time_to_quiz(){
	$('#have_time_to_quiz').click(function(){
		if($('#have_time_to_quiz:checked').length > 0){
			$('.time_to_quiz').removeClass('hidden').addClass('show');
		} else {
			$('.time_to_quiz').removeClass('show').addClass('hidden');
		}
	});	
}

function add_more_different_answer(){

	$('input[name="num_answer"]').val($('input[name^="answer_text"]').length);
	
	$('.quiz_input .add_more').click(function(){
		if ($('input[name^="answer_text"]').length < 4) {
			n = $('input[name^="answer_text"]').length;
			var $element= $('<div class="form-group"><input class="form-control" placeholder="Nhập câu trả lời đúng khác vào đây" name="answer_text_'+ n +'" type="text"></div>');
			$element.appendTo($('.different-answers'));

			$('input[name="num_answer"]').val($('input[name^="answer_text"]').length);
		} else {
			var $element = $('<p class="alert alert-warning">Bạn chỉ được nhập tối đa 3 đáp án khác.</p>');
			$element.appendTo($('.different-answers'));
		}
		
	});
}


function admin_sidebar_menu(){
	var $nav_name = $('.sidebar-menu').find('.header').text();
	$('.sidebar-menu').find('.header a').remove();
	$('.sidebar-menu').find('.header').text($nav_name);
	$('.sidebar-menu > li').each(function(){
		$(this).find('> ul').addClass('treeview-menu');
		var $icon = $('<i class="fa fa-circle-o"></i>');
		$( '<i class="fa fa-circle-o"></i>' ).prependTo( $(this).find('> ul > li > a') );
	});
}

function show_thumbnail(){
	var $thumbnail = $('#thumbnail_old').text();
	if ($thumbnail != "") {
		$('.image-holder').addClass('show');
		$('.image-holder').html('<img src="'+ $thumbnail +'"/>');
	}
}

