jQuery( document ).ready(function($) {
	loadMorePostHome();

	getResultQuiz();
	
    monkeyPatchAutocomplete();
	searchVocabularySuggest();


	$('.widget-practive .practive-single').click(function(){
        var $this = $(this);
        $this.append('<i class="fa fa-spinner fa-spin" style="margin-left: 5px;"></i>');
		groupQuizId = $('#group_quiz_id').val();
		$.ajax({
            url: 'http://localhost:8000/ajaxGetQuizzes',
            type: 'get',
            data: 
            {
                groupQuizId: groupQuizId
            },
            success: function(data)
            {
                $this.find('.fa').remove();
                
                var htmlItem = "";
                var jsonResult = {"result":[]};
                $jsonDataQuiz = JSON.parse(data);

                $('#modalPostQuiz .modal-title').text($jsonDataQuiz.group_quiz.title);
                $('#modalPostQuiz .modal-body').append('<ul class="list-group"></ul>');
                if ($jsonDataQuiz.group_quiz.quizs.length > 0) {
                    var $scoreQuiz = 0;
                    var $scorePlus = $('#modalPostQuiz .progress-bar').attr('aria-valuemax') / $jsonDataQuiz.group_quiz.quizs.length;
                    var $arIndexQuiz = [];
                    for (var i = 0; i < $jsonDataQuiz.group_quiz.quizs.length; i++) {
                        $arIndexQuiz.push(i);
                    }
                    writeQuizItem($jsonDataQuiz, $arIndexQuiz);
                    $('#modalPostQuiz').modal({
                        'show': true,
                        'backdrop': 'static',
                        'keyboard': false
                    });

                    $('#modalPostQuiz .btn-primary').click(function(e){
                        e.preventDefault();
                        if ($(this).hasClass('check')) {
                            var $idQuiz = $('#modalPostQuiz .modal-body .list-group-item').data('index');
                            var $nameAnswer = 'answer_quiz_' + $idQuiz;
                            var $val = 0;
                            if ($jsonDataQuiz.group_quiz.quizs[$idQuiz].type == 'select') 
                            {
                                $("#modalPostQuiz .modal-body [type='radio']").each(function(){
                                    if ($(this).prop('checked')) {
                                        $val = $(this).val();
                                    }
                                });

                                if ($jsonDataQuiz.group_quiz.quizs[$idQuiz].answers[$val].is_correct == 1) {
                                    $('.result-status').removeClass('hide').text('Đúng');

                                    if($arIndexQuiz.indexOf($idQuiz) != -1) {
                                        $arIndexQuiz.splice($arIndexQuiz.indexOf($idQuiz), 1);
                                    }
                                    $scoreQuiz += $scorePlus;
                                    $('#modalPostQuiz .progress-bar').css('width', $scoreQuiz + "%");
                                    jsonResult.result.push({ "quiz_id": $jsonDataQuiz.group_quiz.quizs[$idQuiz].id, 
                                                            "answer_id": $jsonDataQuiz.group_quiz.quizs[$idQuiz].answers[$val].id });

                                    ajaxPlusScore($arIndexQuiz, $scoreQuiz, jsonResult, $scorePlus, groupQuizId, 15);
                                } else {
                                    $('.result-status').removeClass('hide').text('Sai');
                                    $('.result-true-label').removeClass('hide');
                                    for (var i = 0; i < $jsonDataQuiz.group_quiz.quizs[$idQuiz].answers.length; i++) {
                                        if ($jsonDataQuiz.group_quiz.quizs[$idQuiz].answers[i].is_correct == 1) {
                                            $('.result-true').text($jsonDataQuiz.group_quiz.quizs[$idQuiz].answers[i].title);
                                            break;
                                        }
                                    }                            
                                }
                            }     
                            else
                            {
                                $val = $("#modalPostQuiz .modal-body [type='text']").val().trim();

                                for (var i = 0; i < $jsonDataQuiz.group_quiz.quizs[$idQuiz].answers.length; i++) {
                                    if ($val == $jsonDataQuiz.group_quiz.quizs[$idQuiz].answers[i].title) 
                                    {
                                        $('.result-status').removeClass('hide').text('Đúng');

                                        if($arIndexQuiz.indexOf($idQuiz) != -1) {
                                            $arIndexQuiz.splice($arIndexQuiz.indexOf($idQuiz), 1);
                                        }
                                        $scoreQuiz += $scorePlus;
                                        $('#modalPostQuiz .progress-bar').css('width', $scoreQuiz + "%");
                                        jsonResult.result.push({ "quiz_id": $jsonDataQuiz.group_quiz.quizs[$idQuiz].id, 
                                                                    "answer_id": $jsonDataQuiz.group_quiz.quizs[$idQuiz].answers[i].id,
                                                                    "answer_from_user" : $val
                                                                });

                                        ajaxPlusScore($arIndexQuiz, $scoreQuiz, jsonResult, $scorePlus, groupQuizId, 15);
                                        break;   
                                    }
                                    else 
                                    {
                                        if (i == ($jsonDataQuiz.group_quiz.quizs[$idQuiz].answers.length -1)) 
                                        {
                                            $('.result-status').removeClass('hide').text('Sai');
                                            $('.result-true-label').removeClass('hide');
                                            for (var i = 0; i < $jsonDataQuiz.group_quiz.quizs[$idQuiz].answers.length; i++) {
                                                if ($jsonDataQuiz.group_quiz.quizs[$idQuiz].answers[i].is_correct == 1) {
                                                    $('.result-true').text($jsonDataQuiz.group_quiz.quizs[$idQuiz].answers[i].title);
                                                }
                                            } 
                                        }
                                    }
                                }
                            }
                            $('#modalPostQuiz .btn-primary').addClass('checked').removeClass('check').text('Câu tiếp');
                            $('#modalPostQuiz .btn-default').addClass('disabled');                    
                        } else if($(this).hasClass('checked')) {
                            reInitQuizItem($jsonDataQuiz, $arIndexQuiz);
                        }    
                    });

                    $('#modalPostQuiz .btn-primary').click(function(){
						if ($(this).hasClass('re-practive')) {
							$scoreQuiz = 0;
							$arIndexQuiz.length = 0;
							for (var i = 0; i < $jsonDataQuiz.group_quiz.quizs.length; i++) {
		                        $arIndexQuiz.push(i);
		                    }
		                    $('#modalPostQuiz .progress-bar').css('width', "0%");
		                    $('#modalPostQuiz .modal-body .notify').remove();
		                    $('#modalPostQuiz .modal-body').append('<ul class="list-group"></ul>');
		                    writeQuizItem($jsonDataQuiz, $arIndexQuiz);
		                    $(this).removeClass('re-practive').addClass('check').text('Kiểm tra');
							$('#modalPostQuiz .btn-default').removeClass('disabled').removeClass('hide');
						}
					});
					$('#modalPostQuiz .btn-default').click(function(){
						$('#modalPostQuiz .btn-default').addClass('disabled');
						$('#modalPostQuiz .btn-primary').addClass('checked').removeClass('check').text('Câu tiếp');
						$('.result-true-label').removeClass('hide');
						var $idQuiz = $('#modalPostQuiz .modal-body .list-group-item').data('index');
                        for (var i = 0; i < $jsonDataQuiz.group_quiz.quizs[$idQuiz].answers.length; i++) {
                            if ($jsonDataQuiz.group_quiz.quizs[$idQuiz].answers[i].is_correct == 1) {
                                $('.result-true').text($jsonDataQuiz.group_quiz.quizs[$idQuiz].answers[i].title);
                                break;
                            }
                        }
					})
                }
            }
        });
	});

	$('.widget-practive .practive-all').click(function(){
		var $this = $(this);
        $this.append('<i class="fa fa-spinner fa-spin" style="margin-left: 5px;"></i>');
		$.ajax({
            url: 'http://localhost:8000/ajaxGetAllQuizzes',
            type: 'get',
            success: function(data)
            {
            	$this.find('.fa').remove();
                
                var htmlItem = "";
                var jsonResult = {"result":[]};
                $jsonDataQuiz = JSON.parse(data);

                $('#modalPostQuiz .modal-title').text($jsonDataQuiz.group_quiz.title);
                $('#modalPostQuiz .modal-body').append('<ul class="list-group"></ul>');
                if ($jsonDataQuiz.group_quiz.quizs.length > 0) {
                    var $scoreQuiz = 0;
                    var $scorePlus = $('#modalPostQuiz .progress-bar').attr('aria-valuemax') / $jsonDataQuiz.group_quiz.quizs.length;
                    var $arIndexQuiz = [];
                    for (var i = 0; i < $jsonDataQuiz.group_quiz.quizs.length; i++) {
                        $arIndexQuiz.push(i);
                    }
                    writeQuizItem($jsonDataQuiz, $arIndexQuiz);
                    $('#modalPostQuiz').modal({
                        'show': true,
                        'backdrop': 'static',
                        'keyboard': false
                    });

                    $('#modalPostQuiz .btn-primary').click(function(e){
                        e.preventDefault();
                        if ($(this).hasClass('check')) {
                            var $idQuiz = $('#modalPostQuiz .modal-body .list-group-item').data('index');
                            var $nameAnswer = 'answer_quiz_' + $idQuiz;
                            var $val = 0;
                            if ($jsonDataQuiz.group_quiz.quizs[$idQuiz].type == 'select') 
                            {
                                $("#modalPostQuiz .modal-body [type='radio']").each(function(){
                                    if ($(this).prop('checked')) {
                                        $val = $(this).val();
                                    }
                                });

                                if ($jsonDataQuiz.group_quiz.quizs[$idQuiz].answers[$val].is_correct == 1) {
                                    $('.result-status').removeClass('hide').text('Đúng');

                                    if($arIndexQuiz.indexOf($idQuiz) != -1) {
                                        $arIndexQuiz.splice($arIndexQuiz.indexOf($idQuiz), 1);
                                    }
                                    $scoreQuiz += $scorePlus;
                                    $('#modalPostQuiz .progress-bar').css('width', $scoreQuiz + "%");
                                    jsonResult.result.push({ "quiz_id": $jsonDataQuiz.group_quiz.quizs[$idQuiz].id, 
                                                            "answer_id": $jsonDataQuiz.group_quiz.quizs[$idQuiz].answers[$val].id });

                                    ajaxPlusScore($arIndexQuiz, $scoreQuiz, jsonResult, $scorePlus, 0, 15);
                                } else {
                                    $('.result-status').removeClass('hide').text('Sai');
                                    $('.result-true-label').removeClass('hide');
                                    for (var i = 0; i < $jsonDataQuiz.group_quiz.quizs[$idQuiz].answers.length; i++) {
                                        if ($jsonDataQuiz.group_quiz.quizs[$idQuiz].answers[i].is_correct == 1) {
                                            $('.result-true').text($jsonDataQuiz.group_quiz.quizs[$idQuiz].answers[i].title);
                                            break;
                                        }
                                    }                            
                                }
                            }     
                            else
                            {
                                $val = $("#modalPostQuiz .modal-body [type='text']").val().trim();

                                for (var i = 0; i < $jsonDataQuiz.group_quiz.quizs[$idQuiz].answers.length; i++) {
                                    if ($val == $jsonDataQuiz.group_quiz.quizs[$idQuiz].answers[i].title) 
                                    {
                                        $('.result-status').removeClass('hide').text('Đúng');

                                        if($arIndexQuiz.indexOf($idQuiz) != -1) {
                                            $arIndexQuiz.splice($arIndexQuiz.indexOf($idQuiz), 1);
                                        }
                                        $scoreQuiz += $scorePlus;
                                        $('#modalPostQuiz .progress-bar').css('width', $scoreQuiz + "%");
                                        jsonResult.result.push({ "quiz_id": $jsonDataQuiz.group_quiz.quizs[$idQuiz].id, 
                                                                    "answer_id": $jsonDataQuiz.group_quiz.quizs[$idQuiz].answers[i].id,
                                                                    "answer_from_user" : $val
                                                                });

                                        ajaxPlusScore($arIndexQuiz, $scoreQuiz, jsonResult, $scorePlus, 0, 15);
                                        break;   
                                    }
                                    else 
                                    {
                                        if (i == ($jsonDataQuiz.group_quiz.quizs[$idQuiz].answers.length -1)) 
                                        {
                                            $('.result-status').removeClass('hide').text('Sai');
                                            $('.result-true-label').removeClass('hide');
                                            for (var i = 0; i < $jsonDataQuiz.group_quiz.quizs[$idQuiz].answers.length; i++) {
                                                if ($jsonDataQuiz.group_quiz.quizs[$idQuiz].answers[i].is_correct == 1) {
                                                    $('.result-true').text($jsonDataQuiz.group_quiz.quizs[$idQuiz].answers[i].title);
                                                }
                                            } 
                                        }
                                    }
                                }
                            }
                            $('#modalPostQuiz .btn-primary').addClass('checked').removeClass('check').text('Câu tiếp');
                            $('#modalPostQuiz .btn-default').addClass('disabled');                    
                        } else if($(this).hasClass('checked')) {
                            reInitQuizItem($jsonDataQuiz, $arIndexQuiz);
                        }    
                    });

                    $('#modalPostQuiz .btn-primary').click(function(){
						if ($(this).hasClass('re-practive')) {
							$scoreQuiz = 0;
							$arIndexQuiz.length = 0;
							for (var i = 0; i < $jsonDataQuiz.group_quiz.quizs.length; i++) {
		                        $arIndexQuiz.push(i);
		                    }
		                    $('#modalPostQuiz .progress-bar').css('width', "0%");
		                    $('#modalPostQuiz .modal-body .notify').remove();
		                    $('#modalPostQuiz .modal-body').append('<ul class="list-group"></ul>');
		                    writeQuizItem($jsonDataQuiz, $arIndexQuiz);
		                    $(this).removeClass('re-practive').addClass('check').text('Kiểm tra');
							$('#modalPostQuiz .btn-default').removeClass('disabled').removeClass('hide');
						}
					});
					$('#modalPostQuiz .btn-default').click(function(){
						$('#modalPostQuiz .btn-default').addClass('disabled');
						$('#modalPostQuiz .btn-primary').addClass('checked').removeClass('check').text('Câu tiếp');
						$('.result-true-label').removeClass('hide');
						var $idQuiz = $('#modalPostQuiz .modal-body .list-group-item').data('index');
                        for (var i = 0; i < $jsonDataQuiz.group_quiz.quizs[$idQuiz].answers.length; i++) {
                            if ($jsonDataQuiz.group_quiz.quizs[$idQuiz].answers[i].is_correct == 1) {
                                $('.result-true').text($jsonDataQuiz.group_quiz.quizs[$idQuiz].answers[i].title);
                                break;
                            }
                        }
					})
                }
            }
        });
	});
	
});
function getResultQuiz(){
	var obj_result = {};
	var name;
	$('.quiz-carousel input').change(function(){
		name = $(this).attr('name');
		obj_result[name] = $(this).val();
	});
	$('.btn-primary').click(function(event){
		obj_result = JSON.stringify(obj_result);
		$("input[name='ar_result']").val(obj_result);
	})
	
}

function searchVocabularySuggest(){
    $.ajax({
        url: 'http://localhost:8000/searchSuggest',
        type:'GET',
        success: function(data){
            $(".search_form input.form-control").autocomplete({
                autoFocus: true,
                source: function(req, responseFn) {
                    var re = $.ui.autocomplete.escapeRegex(req.term);
                    console.log(re);
                    var matcher = new RegExp(re, "gi");
                    console.log(matcher);
                    var a = $.grep( data, function(item,index){
                        return matcher.test(item);
                    });
                    responseFn( a );
                }
            });  
        }
    });	   
}

function monkeyPatchAutocomplete() {
  var oldFn = $.ui.autocomplete.prototype._renderItem;
  $.ui.autocomplete.prototype._renderItem = function( ul, item) {
        ul.addClass('.list-group');
        var re = new RegExp(this.term, "gi") ;
        var t = item.label.replace(re,"<strong>" + this.term + "</strong>");
        return $( "<li></li>" )
          .data( "item.autocomplete", item )
          .append( "<a>" + t + "</a>" )
          .addClass('list-group-item')
          .appendTo( ul );
  };
}

function loadMorePostHome() {
	$('.home_page #load-more').click(function(even){
        var $this = $(this).find('.fa');
		even.preventDefault();
        $this.addClass('fa-spin').addClass('fa-spinner');
		var lastPostId = $('.widget-list-items .row > div:last .list-item').data('id');
        var postType = $('.widget-list-items').data('type');   
        var userId =  $('.widget-list-items').data('user-id');   
	    $.ajax({
	        url: 'http://localhost:8000/load_more_post',
	        type: 'GET',
	        data: { 
                lastPostId: lastPostId ,
                postType: postType,
                userId: userId
            },
	        success: function(data)
	        {                
                $this.removeClass('fa-spin').removeClass('fa-spinner');
	            var listArticles = data;

	            if (listArticles.length > 0) {
			        var html = "";
			        
			        for (var i = 0; i < listArticles.length; i++) {
			        	html += '<div class="col-md-4">';
			        	html += '<div class="list-item" data-id="'+ listArticles[i]["id"] +'" data-type="'+ listArticles[i]["post_type"] +'">';
						html += '<a href="http://localhost:8000/post/'+ listArticles[i]["id"] +'" class="list-item-thumbnail">';
						if (listArticles[i]["post_thumbnail"]) {
							html += '<img src="'+ listArticles[i]["post_thumbnail"] +'" alt="'+ listArticles[i]["post_title"] +'">';
							
						} else {
							html += '<img src="http://localhost:8000/images/default/320x180.png" alt="'+ listArticles[i]["post_title"] +'">';
						}	
						if (listArticles[i]["youtube_id"]) 
						{
							html += '<i class="fa fa-play icon-play"></i>';
						}
						html += '</a>';
						html += '<h3 class="list-item-title"><a href="http://localhost:8000/post/'+ listArticles[i]["id"] +'" title="'+ listArticles[i]["post_title"] +'">'+ listArticles[i]["post_title"] +'</a></h3>';
						html += '</div>';
						html += '</div>';
			        }
			        $('.widget-list-items .row').append($(html));
		        } else {
		        	$('.home_page #load-more').remove();
		        }
	        }
	    });
	});
}


function ajaxPlusScore($arIndexQuiz, $scoreQuiz, jsonResult, $scorePlus, groupQuizId, scoreMax) {
    if ($scoreQuiz  == (scoreMax * $scorePlus) || $arIndexQuiz.length == 0) { 
        $.ajax({
            url: 'http://localhost:8000/ajaxResultQuiz',
            type: 'get',
            data: 
            {
                jsonResult: jsonResult,
                groupQuizId: groupQuizId
            },
            success: function(data)
            {
                $('#modalPostQuiz .modal-body').empty().append("<p class='notify'>" + data.notify + "</p>");
                $('.result-true-label').addClass('hide');
                $('.result-true').text("");
                $('.result-status').addClass('hide');
                $('#modalPostQuiz .btn-default').addClass('hide');
                $('#modalPostQuiz .btn-primary').removeClass('checked').addClass('re-practive').text("Luyện tập lại");
            }
        });
    }
}

function reInitQuizItem($jsonDataQuiz, $arIndexQuiz){
    $('#modalPostQuiz .modal-body .list-group').empty();
    $('.result-true-label').addClass('hide');
    $('.result-true').text("");
    $('.result-status').addClass('hide');
    writeQuizItem($jsonDataQuiz, $arIndexQuiz);
    $('#modalPostQuiz .btn-primary').removeClass('checked').addClass('check').text('Kiểm tra');
    $('#modalPostQuiz .btn-default').removeClass('disabled');
}
function writeQuizItem($jsonDataQuiz, $arIndexQuiz) {
    var $randomIndex = $arIndexQuiz[Math.floor(Math.random() * $arIndexQuiz.length)];
    htmlItem = "";
    htmlItem += "<li class='list-group-item' data-index="+ $randomIndex +">";
    htmlItem += "<h3 class='quiz-title'>"+ $jsonDataQuiz.group_quiz.quizs[$randomIndex].title +"</h3>";
    htmlItem += "<div class='row'>";                

    if ($jsonDataQuiz.group_quiz.quizs[$randomIndex].type == 'select') {
        for (var j = 0; j < $jsonDataQuiz.group_quiz.quizs[$randomIndex].answers.length; j++) {
            htmlItem += "<div class='col-md-3'>";
            htmlItem += "<div class='item' >";
            htmlItem += "<label>";            
            htmlItem += "<div class='item-thumbnail'>";
            htmlItem += "<img src='" + $jsonDataQuiz.group_quiz.quizs[$randomIndex].answers[j].thumbnail + "'/>";
            htmlItem += "</div>";
            htmlItem += "<h4 class='quiz-title'>"+ $jsonDataQuiz.group_quiz.quizs[$randomIndex].answers[j].title +"</h4>";
            htmlItem += "<input type='radio' name='answer_quiz_"+ $jsonDataQuiz.group_quiz.quizs[$randomIndex].id +"' value='" + j + "'/>";
            htmlItem += "</label>";
            htmlItem += "</div>";
            htmlItem += "</div>";
        }    
    } else {
        htmlItem += "<div class='col-md-12'>";
        htmlItem += "<input type='text' name='answer_quiz_"+ $jsonDataQuiz.group_quiz.quizs[$randomIndex].id +"' class='form-control' />"
        htmlItem += "</div>";
    }
    
    htmlItem += "</div>";
    htmlItem += "</li>";
    $(htmlItem).appendTo($('#modalPostQuiz .modal-body .list-group'));
}


var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope) {
    $scope.firstName= "John";
    $scope.lastName= "Doe";
});