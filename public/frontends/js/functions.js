jQuery( document ).ready(function($) {
	$('#primary-menu').superfish();

	/**
	 * Time line
	 */

	var timelineBlocks = $('.cd-timeline-block'),
		offset = 0.8;

	//hide timeline blocks which are outside the viewport
	hideBlocks(timelineBlocks, offset);

	//on scolling, show/animate timeline blocks when enter the viewport
	$(window).on('scroll', function(){
		(!window.requestAnimationFrame) 
			? setTimeout(function(){ showBlocks(timelineBlocks, offset); }, 100)
			: window.requestAnimationFrame(function(){ showBlocks(timelineBlocks, offset); });
	});

	function hideBlocks(blocks, offset) {
		blocks.each(function(){
			( $(this).offset().top > $(window).scrollTop()+$(window).height()*offset ) && $(this).find('.cd-timeline-img, .cd-timeline-content').addClass('is-hidden');
		});
	}

	function showBlocks(blocks, offset) {
		blocks.each(function(){
			( $(this).offset().top <= $(window).scrollTop()+$(window).height()*offset && $(this).find('.cd-timeline-img').hasClass('is-hidden') ) && $(this).find('.cd-timeline-img, .cd-timeline-content').removeClass('is-hidden').addClass('bounce-in');
		});
	}

	/**
	 * back to top
	 */

	var scrollTrigger = 100; // px
	backToTop = function () {
	    var scrollTop = $(window).scrollTop();
	    if (scrollTop > scrollTrigger) {
	        $('#back-to-top').addClass('active');
	    } else {
	        $('#back-to-top').removeClass('active');
	    }
	};
	backToTop();
	$(window).on('scroll', function () {
	    backToTop();
	});
	$('#back-to-top').on('click', function (e) {
	    e.preventDefault();
	    $('html,body').animate({
	        scrollTop: 0
	    }, 700);
	});


	/**
	 * 
	 * @param   quiz_carousel()
	 * @return    
	 */
	$('#accordion').on('shown.bs.collapse', function () {
		quiz_carousel();
	})
	function quiz_carousel(){
		$(".quiz-carousel").owlCarousel({
			singleItem:true,
			pagination:false,
			slideSpeed:500
		});

		var owl = $(".quiz-carousel").data('owlCarousel');
		$('.carousel-controls .prev-carousel').click(function(event){
			event.preventDefault();
			owl.prev();
		});
		$('.carousel-controls .next-carousel').click(function(event){
			event.preventDefault();
			owl.next();
		});
	}
	scrollSub();
	function scrollSub() {
		if ($('.detail_page .sub-item .panel-body .panel-body').length > 0) {
			$('.detail_page .sub-item .panel-body .panel-body').mCustomScrollbar({
				theme:"dark"
			})
		}		
	}

});